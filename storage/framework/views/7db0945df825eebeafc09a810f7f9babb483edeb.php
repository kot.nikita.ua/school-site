
<?php $__env->startSection('bread'); ?>
<? $user = Auth::user(); ?>
<li><a href="/upgradedusers">Список <?php if($user->role_id == 4): ?>вчителів <?php endif; ?> <?php if($user->role_id == 5): ?>адміністраторів <?php endif; ?> <?php if($user->role_id == 1): ?>користувачів <?php endif; ?> </a></li>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('name'); ?>
Список <?php if($user->role_id == 4): ?>вчителів <?php endif; ?> <?php if($user->role_id == 5): ?>адміністраторів <?php endif; ?> <?php if($user->role_id == 1): ?>користувачів <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container" id="fullsc">
    

<div class="col-70" style="margin-top: 20px; margin-bottom:20px;">

                    
                <div class="content-course-list">
                    <?php $__empty_1 = true; $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upgrade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    
                       
                   
                   <div class="flat-course clearfix" style="padding: 20px;">
                        <div class="featured-post">
                            <div class="entry-image" style="margin-left: 20px;">
                                <img src="<?php echo e(asset('storage/')); ?>\<?php echo e($upgrade->avatar); ?>" alt="images" style="height:100px;width:100px; border-radius:100px; background-color:#028174">
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                   
                                    <a href="#"><?php echo e($upgrade->name); ?></a>
                                   
                                </h4>
                           
                                <p>
                                    Деталі користувача:
                                </p>
                                <p>
                                    Email - <?php echo e($upgrade->email); ?>

                                </p>
                                <p>
                                   Посада - <?
                        switch ($upgrade->role_id) {
                            case 3:
                                $down = 1;
                               echo "Вчитель школи ".$upgrade->school;
                                break;
                            case 4:
                                $down = 1;
                                echo "Адміністратор школи ".$upgrade->school;
                                break;
                            case 5:

                                $down = 0;
                                echo "Глобальний адміністратор";
                                break;
                          
                        }
                                   ?>
                                </p>
                               
                            </div>
                            <div class="wrap-rating-price">
                                
                                    <div class="rating" style="border-right: 0;">
                                        <form method="POST" action="<?php echo e(route('teacherdowngrade')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <?php if($down == 1): ?>
                                            <input type="text" name="school" value="<?php echo e($upgrade->school); ?>"  hidden>
                                            <?php endif; ?>
                                            <input type="text" name="role" value="<?php echo e($upgrade->role_id); ?>"  hidden>
                                            <input type="text" name="email" value="<?php echo e($upgrade->email); ?>"  hidden>
                                            <button class="delete-post" type="submit" name="id" value="<?php echo e($upgrade->id); ?>" >Понизити користувача</button>
                                        </form>
                                        
                                    </div>
                                  
                                
                            </div>
                        </div>
                    
                </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <p>Нема користувачів</p>
                    <?php endif; ?>
                </div>
                <?php echo e($teachers->links('layouts.pagination')); ?>

                
            </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/user-teachers.blade.php ENDPATH**/ ?>