
<?php $__env->startSection('bread'); ?>
<li><a href="/user/upgrade">Заяви</a></li>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('name'); ?>
Заяви на підвищення
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container" id="fullsc">
    

<div class="col-70" style="margin-top: 20px; margin-bottom:20px;">

                    
                <div class="content-course-list">
                    <?php $__empty_1 = true; $__currentLoopData = $upgrades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upgrade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    
                       
                   
                   <div class="flat-course clearfix" style="padding: 20px;">
                        <div class="featured-post">
                            <div class="entry-image" style="margin-left: 20px;">
                                <img src="<?php echo e(asset('storage/')); ?>\<?php echo e($upgrade->avatar); ?>" alt="images" style="height:100px;width:100px; border-radius:100px; background-color:#028174">
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                   
                                    <a href="#">Заява на підвищення</a>
                                   
                                </h4>
                                <p>
                                    Підвищення до : <?php if($upgrade->role == 'teacher'): ?> Вчителя Школи №:<?php echo e($upgrade->school); ?> <?php elseif($upgrade->role == 'admin'): ?>Адміністратор Школи №:<?php echo e($upgrade->school); ?><?php elseif($upgrade->role == 'globaladmin'): ?> Глобальный Адміністратор <?php endif; ?>
                                </p>
                                <p>
                                    Деталі заяви: номер телефону - <?php echo e($upgrade->phone); ?> , Email - <?php echo e($upgrade->email); ?>

                                </p>
                                <div class="author-info">
                                    <div class="author-name">
                                        <?php echo e($upgrade->name); ?>

                                    </div>
                                    <div class="author-name" style="margin-left: 10px;">
                                       <span class="price-now">  <?php echo e(date('j/n/Y', strtotime($upgrade->date))); ?></span>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">
                                        <form method="POST" action="<?php echo e(route('deletereq')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <input type="text" name="email" value="<?php echo e($upgrade->email); ?>"  hidden>
                                            <button class="delete-post" type="submit" name="post" value="<?php echo e($upgrade->id); ?>" >Видалити Заяву</button>
                                        </form>
                                        
                                    </div>
                                    <div class="price">
                                    <form method="POST" action="<?php echo e(route('aprovereq')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <input type="text" name="email" value="<?php echo e($upgrade->email); ?>"  hidden>
                                            <button class="aprove-post"  type="submit" name="post" value="<?php echo e($upgrade->id); ?>" >Пітвердити Заяву</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <p>Нема Заяв</p>
                    <?php endif; ?>
                </div>
                <?php echo e($upgrades->links('layouts.pagination')); ?>

                
            </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/user-requests.blade.php ENDPATH**/ ?>