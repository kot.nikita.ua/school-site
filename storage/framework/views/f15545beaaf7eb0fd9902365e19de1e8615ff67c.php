<footer id="footer" class="footer-type2">
    <?php if(Request::is('about')): ?>
        <div id="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-footer h-100 align-self-center">
                        <div class="logo-footer my-auto text-center">
                            <img src="<?php echo e(asset('assets/images/logo/02.png')); ?>" style="width: 170px; height:40px;" alt="images">
                        </div>
                    </div>
                    <div class="col-lg-6 col-link text-center">
                        <h3 class="widget widget-title text-white">
                            Допоміжні посилання
                        </h3>
                        <ul class="widget-nav-menu">
                            <li><a href="https://iabs.academy/" target="_blank">IABS</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div id="bottom" class="bottom-type2 clearfix has-spacer">
        <div id="bottom-bar-inner" class="container">
            <div class="bottom-bar-inner-wrap">
                <div class="bottom-bar-content">
                    <div id="copyright">
                        ©
                        <span id="text-year" class="text-year">
                                2020
                            </span>
                        <span class="text-name">
                                Onion, IABS
                            </span>
                        <span class="license">
                                Всі права захищено
                            </span>
                    </div>
                </div>
                <div class="bottom-bar-menu">
                    <ul class="bottom-nav">
                        <li class="menu-item"><a href="<?php echo e(route('about')); ?>/#about-developers">Про розробників</a></li>
                        <li class="menu-item"><a href="<?php echo e(route('about')); ?>/#contact-form">Залишити відгук</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <a id="scroll-top" class="show"></a>
</footer>
<!-- footer -->
<?php /**PATH C:\OpenServer\OpenServer\domains\ProJectSchool\resources\views/layouts/footer.blade.php ENDPATH**/ ?>