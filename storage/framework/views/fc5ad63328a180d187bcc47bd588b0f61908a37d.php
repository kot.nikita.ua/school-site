
<?php $__env->startSection('content'); ?>
<div class="container">
    

<div class="col-70" style="margin-top: 20px; margin-bottom:20px;">

                    <div class="switch-layout" style="margin-bottom: 20px;">
                        
                        <button  onclick="window.location.href='/admin/posts/create'">Додати Новину<img src="<?php echo e(asset('assets\icons\plus.png')); ?>" alt="" style="margin-left:5px; height: 20px;width:20px; "></button>
                    </div>
                <div class="content-course-list">
                    <?php $__empty_1 = true; $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newsone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="flat-course clearfix">
                        <div class="featured-post">
                            <div class="entry-image">
                                <img src="storage/<?php echo e($newsone->image); ?>" alt="images" style="height:270px;width:370px;">
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    <?php if($new == 'local'): ?>
                                    <a href="/school-news/<?php echo e($newsone->id); ?>"><?php echo e($newsone->title); ?></a>
                                    <?php else: ?>
                                    <a href="/news/<?php echo e($newsone->id); ?>"><?php echo e($newsone->title); ?></a>
                                    <?php endif; ?>
                                </h4>
                                <p>
                                    <?php echo e($newsone->excerpt); ?> 
                                </p>
                                <div class="author-info">
                                    <div class="author-name">
                                        <?php echo e($newsone->name); ?>

                                    </div>
                                    <div class="enroll">
                                        <a href="/admin/posts/<?php echo e($newsone->id); ?>/edit">Редагувати</a>
                                    </div>
                                </div>
                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">
                                        <form method="POST" action="<?php echo e(route('deletepost')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <button class="delete-post" name="post" value="<?php echo e($newsone->id); ?>">Видалити</button>
                                        </form>
                                        
                                    </div>
                                    <div class="price">
                                    <?php if($new == 'local'): ?>  
                                    <span class="price-now"><p id="school"> Школа №:<span> <?php echo e($newsone->school); ?> </span></p></span>
                                        <span class="price-now"><p id="class"> Клас: <span> <?php echo e($newsone->class); ?> </span></p></span>
                                        <?php else: ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <p>Нема Новин</p>
                    <?php endif; ?>
                </div>
                <?php echo e($news->links('layouts.pagination')); ?>

                
            </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\treble\mentor\OSPanel\domains\ProJectSchool\resources\views/user-posts.blade.php ENDPATH**/ ?>