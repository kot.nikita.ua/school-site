
<?php $__env->startSection('content'); ?>
<div class="blog-bl content-blog" style="margin-top: 20px;">
        <div class="container">
            <script>
               
               
                var counter = Number("12");
                var clas3 = 'all';
                var school3 = "<?php echo e($schoolall); ?>";
               
            </script>   
            <div class="row" >
            
            <div class="col-lg-8" >
                    <div id="refresh">
                        <?php echo $__env->make('school-news-ajax', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                     
                </div>
               
                <div class="col-lg-4">
                    <div class="sidebar">
                        <div class="widget widget-search">
                            <div class="search-blog-wrap">
                                <div  class="search-form">
                                    <input type="search" id="search" placeholder="Шукати тут ....">
                                    <button class="search-button" id="button">
                                        <i class="fa fa-search" aria-hidden="true"></i> 
                                    </button>
</div>
                            </div>
                        </div>
                        <div class="widget widget-products">
                        <div class="widget-title">
                            Вибрати школу
                            <span class="bg">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="content">
                            <ul>
                            <li><a href="#refresh" id="all" >  <p id="school">Всі Школи</p></a></li>
                                <script>
                                    var schoolall = document.getElementById("all");
                                    schoolall.onclick = function(){
                                         school3 = "<?php echo e($schoolall); ?>";
                                        var text = $('#search').val();
                                       
                                        search(text,counter,school3,clas3);
                                       
                                    };
                                </script>
                                <?php $__currentLoopData = $search; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $school): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="#refresh" id="<?php echo e($school); ?>" >  <p id="school">№ <?php echo e($school); ?></p></a></li>
                                <script>
                                    var school<?php echo e($school); ?> = document.getElementById("<?php echo e($school); ?>");
                                    school<?php echo e($school); ?>.onclick = function(){
                                         school3 = "<?php echo e($school); ?>";
                                        var text = $('#search').val();
                                       
                                        search(text,counter,school3,clas3);
                                       
                                    };
                                </script>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        </div>
                        <div class="widget widget-products">
                        <div class="widget-title">
                            Вибрати Клас
                            <span class="bg">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="content">
                            <ul>
                            <li><a href="#refresh" id="alll" >  <p id="class">Всі класи</p></a></li>
                                <script>
                                    var clasalll = document.getElementById("alll");
                                    clasalll.onclick = function(){
                                        clas3 = 'all'
                                        var text = $('#search').val();
                                        
                                        search(text,counter,school3,clas3);
                                      
                                    };
                                </script>
                                <?php $__currentLoopData = $classSearch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="#refresh" id="<?php echo e($class); ?>" >  <p id="class">: <?php echo e($class); ?></p></a></li>
                                <script>
                                    var с<?php echo e($class); ?> = document.getElementById("<?php echo e($class); ?>");
                                        с<?php echo e($class); ?>.onclick = function(){
                                         clas3 = "<?php echo e($class); ?>";
                                        var text = $('#search').val();
                                       
                                        search(text,counter,school3,clas3);
                                      
                                    };
                                </script>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                      
                        </div>
                    </div>
                       
                    <?php echo $__env->make('lastnews', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <!---------------------------------- може будет нужна -->
                        <div class="widget widget-instagram-post" hidden>
                            <h4 class="widget-title">
                                <span>Instagram</span>
                            </h4>
                            <div class="news-block">
                                <div class="w-content news-block-content news-block-content-cus">
                                    <ul>
                                        <li><img src="images/blog-sidebar/04.png" alt="images"></li>
                                        <li><img src="images/blog-sidebar/06.png" alt="images"></li>
                                        <li><img src="images/blog-sidebar/06.png" alt="images"></li>
                                        <li><img src="images/blog-sidebar/04.png" alt="images"></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                        <!---------------------------------- може будет нужна -->
                    </div>
                </div>
            </div>
            <script>
                 
                
                 var sear = document.getElementById('button');
               
 sear.onclick = function(){
    var text = $('#search').val();
    search(text,counter,school3,clas3);

 };
 function clas2(clas){
    var text = $('#reqest').val();
    search(text,counter,school3,clas);
 }
 function school2(school){
    var text = $('#reqest').val();
    search(text,counter,school,clas3);
 }
 function search(search,counter,school,clas){
       
        classt = String(clas);
       $.ajax({
        
               url: "<?php echo e(route('schoolnewsSearch')); ?>",
               method: "POST",
               contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data: { Search: search , addBlog: counter ,Class: classt,School:school }
               
             })
               .done (

                   function (data){
                    
              
              $('#refresh').hide().html(data).fadeIn('slower');
               });          
 }
            </script>
        </div>
    </div><!-- content-blog -->
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\treble\mentor\OSPanel\domains\ProJectSchool\resources\views/school-news.blade.php ENDPATH**/ ?>