
<?php $__env->startSection('content'); ?>
<div class="blog-single content-blog" style="margin-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="site-content clearfix">
                    <article class="post post-blog-single">
                        <div class="featured-post post-img">
                            <img src="<?php echo e(asset('storage/')); ?>/<?php echo e($news->image); ?>" alt="images">
                        </div>
                        <div class="content-blog-single">
                            <ul class="social social-blog-single pd-top8">
                                <li><i class="fa fa-facebook" aria-hidden="true"></i></li>

                                <li><i class="fa fa-instagram" aria-hidden="true"></i></li>

                            </ul>
                            <div class="content-blog-single-inner">
                                <div class="content-blog-single-wrap">
                                    <h1 class="title pd-title-single">
                                        <a href="#"> <?php echo e($news->title); ?></a>
                                    </h1>
                                    <!-- контент новости -->
                                    <? echo "{$news->body}"; ?>
                                    <!-- конец контента -->
                                    <div class="blog-single-poster">
                                        <div class="blog-single-poster-wrap">
                                            <div class="avtar-poster">
                                                <img src="<?php echo e(asset('storage/')); ?>/<?php echo e($news->avatar); ?>" alt="images"
                                                    style="width: 90px; height:90px; border-radius: 90px;background-color:#028174">
                                            </div>
                                            <div class="info-poster">
                                                <div class="name">
                                                    <?php echo e($news->name); ?>

                                                </div>
                                                <div class="position">
                                                    <?php echo e(date('j/n/Y', strtotime($news->updated_at))); ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="refresh" >
                                    <?php echo $__env->make('comments', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
                                    
                                </div>
                       
                                <div id="respond" class="comment-respond" style="margin-top: 50px; margin-bottom: 20px;">
    <h3 id="reply-title" class="comment-reply-title mg-bottom24">
        Коментувати
    </h3>
    <?php if(Auth::check()): ?>
    <div class="comments">

        <div class="message-wrap">
            <textarea name="comment" id="comment_text" rows="8" placeholder="Напишіть коментар"></textarea>
        </div>
        <div class="mg-top30">

            <button class="btn btn-post-comment box-shadow-type1" id="addComent">
                Залишити коментар
            </button>



        </div>
    </div>
    <?php else: ?>
    <div class="comments">

        <div class="message-wrap">
            <textarea name="comment" id="comment-message" rows="8" placeholder=" Зареэструйтесь щоб залишити коментар" disabled></textarea>
        </div>
        <div class="mg-top30">



            <button class="btn btn-post-comment box-shadow-type1" disabled>
                Зареэструйтесь щоб залишити коментар
            </button>

        </div>
    </div> 
    <?php endif; ?>
</div>
                               <script >var addd = document.getElementById('addComent');
 addd.onclick = function(){
    var text = $('#comment_text').val();
    var userid = "<? echo( Auth::id()); ?>";
    var postid = "<?php echo e($news->id); ?>";
    add(text,userid,postid);

 };
function add(Text,UserId,postId){
    
       $.ajax({
        
               url: "<?php echo e(route('addcoment')); ?>",
               method: "POST",
               contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data: { text: Text , user: UserId ,post: postId }
               
             })
               .done (

                   function (data){
                    
                     var sc = $('#refresh');
                        dn = $(sc).offset().top;
                        $('html, body').animate({scrollTop: dn}, 500);
                        update();
              
              }
              );          
 }
 function update(){
   var id = "<?php echo e($news->id); ?>";
   $.ajax({
    
           url: "<?php echo e(route('update')); ?>",
           method: "POST",
           contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           data: { id:id }
           
         })
           .done (

               function (data){
                
                $('#refresh').html(data);
               
          }
          );          
}
function deletecomment(coment_id){
   
   $.ajax({
    
           url: "<?php echo e(route('delete')); ?>",
           method: "POST",
           contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           data: { id:coment_id }
           
         })
           .done (

               function (data){
                
                update();
               
          }
          );          
}</script>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="related-posts" style="margin-bottom: 20px;" hidden>
                    <div class="title">
                        Схожі новини
                    </div>
                    <div class="flat-testimonial-carousel">
                        <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="2"
                            data-column2="2" data-column3="1" data-column4="1" data-dots="false" data-auto="false">
                            <div class="owl-carousel">
                                <!---------------------------------- похожая нвовость -->
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="<?php echo e(asset('assets/images/blog-single/02.png')); ?>" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">"Заголовок"</a>
                                                </h5>
                                                <div class="text">
                                                    "новость скороченно"
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Читати далі</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!---------------------------------- конец  похожей нвовости -->
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="<?php echo e(asset('assets/images/blog-single/03.png')); ?>" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">5 Tips To Improve Your Design Skills</a>
                                                </h5>
                                                <div class="text">
                                                    Tips To Improve Your Design Skills. Learn to identify good design.
                                                    If you want to create great designs, first you need to learn.
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Read more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="<?php echo e(asset('assets/images/blog-single/04.png')); ?>" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">5 Tips To Improve Your Design Skills</a>
                                                </h5>
                                                <div class="text">
                                                    Tips To Improve Your Design Skills. Learn to identify good design.
                                                    If you want to create great designs, first you need to learn.
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Read more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="<?php echo e(asset('assets/images/blog-single/05.png')); ?>" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">5 Tips To Improve Your Design Skills</a>
                                                </h5>
                                                <div class="text">
                                                    Tips To Improve Your Design Skills. Learn to identify good design.
                                                    If you want to create great designs, first you need to learn.
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Read more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
                   
                    <?php if(Auth::check()): ?>
                    <?php else: ?>
                    <div class="widget widget-sent">
                        <div class="apply-admission">
                            <div class="apply-admission-wrap type1 bd-type1">
                                <div class="apply-admission-inner">
                                    <h2 class="title text-center">
                                        <span>Бажаете дивитись новини про свою школу?</span>
                                    </h2>
                                    <hr class="hr-side">
                                    <div class="caption text-center text-white"> Зареєструйтесь зараз!
                                    </div>
                                    <div class="apply-sent apply-sent-style1">
                                        <form action="<?php echo e(action('Registration@email')); ?>" method="POST"
                                            class="form-sent">
                                            <input type="Email" placeholder="введіть email ....">
                                            <button class="sent-button bg-cl183251" type="submit" name="Send">
                                                <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php echo $__env->make('lastnews', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <!---------------------------------- може будет нужна -->
                    <div class="widget widget-instagram-post" hidden>
                        <h4 class="widget-title">
                            <span>Instagram</span>
                        </h4>
                        <div class="news-block">
                            <div class="w-content news-block-content news-block-content-cus">
                                <ul>
                                    <li><img src="images/blog-sidebar/04.png" alt="images"></li>
                                    <li><img src="images/blog-sidebar/06.png" alt="images"></li>
                                    <li><img src="images/blog-sidebar/06.png" alt="images"></li>
                                    <li><img src="images/blog-sidebar/04.png" alt="images"></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <!---------------------------------- може будет нужна -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\OpesServer\OSPanel\domains\mentor\resources\views/news-single.blade.php ENDPATH**/ ?>