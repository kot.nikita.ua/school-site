<div style="comments" >
    <h3 class="comments-h">
        Коментарі
    </h3>
    <ul>

        <!-- ------------ коментар -->
<?php $__empty_1 = true; $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <li class="single_comment" id="full<?php echo e($comment->id); ?>">

            <div class="comment-content d-flex">

                <div class="comment-author">
                    <img src="<?php echo e(asset('storage/')); ?>/<?php echo e($comment->avatar); ?>" class="avatarpic"  alt="author">
                </div>

                <div class="comment-meta">
                    <div class="d-flex">
                        <a href="#" class="post-author"><?php echo e($comment->name); ?></a>
                        <a href="#" class="post-date"><?php echo e(date('j/n/Y', strtotime($comment->created_at))); ?> </a>
                        <!-- только для админов или етого пользавотеля -->
                        <?php if(Auth::id() == $comment->user_id): ?>
                        <div class="delete">
                        
                            <input type="image" name="" value="" src="<?php echo e(asset('assets\icons\times-circle.svg')); ?>"
                                class="delete-ico">
                            <button type="button" id="<?php echo e($comment->id); ?>" class="delete-ico-span">Видалити коментар?</button>
                            <script>
                                 var com<?php echo e($comment->id); ?> = document.getElementById("<?php echo e($comment->id); ?>");
                                 com<?php echo e($comment->id); ?>.onclick = function(){
                                   $("#full"+"<?php echo e($comment->id); ?>").hide('fast',function(){
                                        deletecomment("<?php echo e($comment->id); ?>");
                                        });
                                    
                                    };
                                    
                            </script>
                        </div>
                        <?php endif; ?>


                        <!-- ------------ -->
                    </div>
                    <p><?php echo e($comment->text); ?></p>

                </div>
            </div>
        </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <li>
    <div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
        <p>Коментарів ще немає</p>
    </div></li>
    <?php endif; ?>
        <!-- ------------ конец коментара-->
    </ul>
</div>
<hr>
<?php echo e($comments->links('layouts.pagination')); ?><?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/comments.blade.php ENDPATH**/ ?>