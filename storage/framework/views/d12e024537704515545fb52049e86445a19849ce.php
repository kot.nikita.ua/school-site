

<?php $__env->startSection('content'); ?>
<div style="min-width: 100%;min-height:100% ; ">
    

<div class="container3" >
    

                    <div class="apply-admission bg-apply-type2">
                        <div class="apply-admission-wrap type1 bd-type2">
                            <div class="apply-admission-inner">
                                <h2 class="title text-center">
                                    <span>Реестрація</span>
                                </h2>
                               
                            </div>
                        </div>
                        <div class="form-apply">
                            <div class="section-overlay183251"></div>
                            <form  method="POST" action="<?php echo e(route('register')); ?>" class="apply-now">
                                <?php echo csrf_field(); ?>
                                <ul>
                                    <li> <label for="name" class="col-md-4 col-form-label " style="color: white;"><?php echo e(__("Ім'я")); ?></label>


    <input id="name" type="text" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="name" value="<?php echo e(old('name')); ?>" required autocomplete="name" autofocus>

    <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
        <span class="invalid-feedback" role="alert">
            <strong><?php echo e($message); ?></strong>
        </span>
    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
</li>
                                    <li> 
                                    <label for="email" style="color: white;" class="col-md-4 col-form-label "><?php echo e(__('E-Mail Адресса')); ?></label><input id="email" type="email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email" value="<?php echo e($email[0]->email); ?>" required autocomplete="email" autofocus>

<?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
    <span class="invalid-feedback" role="alert">
        <strong><?php echo e($message); ?></strong>
    </span>
<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?></li>   
<li>
<label for="password" style="color: white;" class="col-md-4 col-form-label "><?php echo e(('Пароль')); ?></label>
    <input id="password" type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password"  style="color:aliceblue;" required autocomplete="current-password">

<?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
    <span class="invalid-feedback" role="alert">
        <strong><?php echo e($message); ?></strong>
    </span>
<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?></li>
<li><label for="password-confirm" class="col-md-4 col-form-label " style="color: white;"><?php echo e(__('Підтвердження паролю')); ?></label>


    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
</li>
                           
                                </ul>
                                <div class="btn-50 hv-border text-center">
                                    <button type="submit"  class="btn bg-clfbb545">
                                      Зареєструватися
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div></div>
                </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\treble\mentor\OSPanel\domains\ProJectSchool\resources\views/registration.blade.php ENDPATH**/ ?>