<div id="loading-overlay">
    <div class="loader"></div>
</div>
<div class="bg-header">
        <div class="flat-header-blog">
            <div class="top-bar clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            
                            
                        </div>
                        <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                            
                        </div>
                    </div>
                </div>
            </div>
            <header class="header menu-bar hv-menu-type2">
            <div class="container">
                <div class="menu-bar-wrap clearfix">
                    <div id="logo" class="logo">
                        <a href="<?php echo e(route('home')); ?>"><img src="<?php echo e(asset('assets/images/logo/02.png')); ?>" alt="mentor" style="width: 170px; height: 35px"></a>
                    </div>
                    <div class="mobile-button"><span></span></div>
                    <div class="header-menu">
                        <nav id="main-nav" class="main-nav">
                            <ul class="menu">
                                <li class="menu-item"><a href="<?php echo e(route('home')); ?>">Головна</a></li>
                                <li class="menu-item"><a href="<?php echo e(route('news')); ?>">Новини</a></li>
                                <li class="menu-item"><a href="<?php echo e(route('about-schools')); ?>">Школи</a></li>
                                <?php if(Auth::check()): ?>
                                    <li class="menu-item"><a href="<?php echo e(route('school-news')); ?>">Шкільні новини</a></li>
                                <?php endif; ?>
                                <li class="menu-item"><a href="<?php echo e(route('about')); ?>">Про проект</a></li>
                                <?php if(Route::has('login')): ?>
                        <?php if(auth()->guard()->check()): ?>
                        <li><a href=""><?php echo e($user->name); ?> <img id="avatar" src="<?php echo e(asset('storage/')); ?>\<?php echo e($user->avatar); ?>" alt="" style="width: 30px; height :30px;border-radius: 30px 30px 30px 30px; "></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="/user/<?php echo e(Auth::id()); ?>">Налаштування</a></li>
                                            <?php if($user->role_id == 1): ?>
                                            <li class="menu-item "><a href="/admin">Адмін панель</a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 4 Or $user->role_id == 5 Or $user->role_id == 1): ?>
                                            <li class="menu-item "><a href="/user/upgrade">Заявки</a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 4 ): ?>
                                            <li class="menu-item "><a href="/about-schools/redact">Опис Школи</a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 4 Or $user->role_id == 5 Or $user->role_id == 1): ?>
                                            <li class="menu-item "><a href="/upgradedusers">Список <?php if($user->role_id == 4): ?>вчителів <?php endif; ?> <?php if($user->role_id == 5): ?>адміністраторів <?php endif; ?> <?php if($user->role_id == 1): ?>користувачів <?php endif; ?></a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 3 OR $user->role_id == 4 Or $user->role_id == 5): ?>
                                            <li class="menu-item "><a href="<?php echo e(route('postsview')); ?>">Мої новини</a></li>
                                            <?php endif; ?>
                                            <li class="menu-item "><a href="/logout">Вихід</a></li>
                                        </ul><!-- sub-menu -->


                                    </li>

                    <?php else: ?>
                    <li class="menu-item"><a href="#" id="login">Увійти</a></li>
                      <?php endif; ?>
                      <?php endif; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
            <div class="page-title page-title-blog">
                <div class="page-title-inner">
                    <div class="breadcrumbs breadcrumbs-blog text-left">
                        <div class="container">  
                            <div class="breadcrumbs-wrap">
                                <ul class="breadcrumbs-inner">
                                    <li><a href="/">Головна</a></li>
                                    <?php echo $__env->yieldContent('bread'); ?>
                                </ul>
                                <div class="title">
                                <?php echo $__env->yieldContent('name'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/layouts/header.blade.php ENDPATH**/ ?>