<?php $__env->startComponent('mail::message'); ?>


Ви подали заявку на регістацию на Шкільний Сайт. Натисніть кнопку знизу для того щоб зареєструватися.

<?php $__env->startComponent('mail::button', ['url' => 'https://mentor/registration']); ?>
Зареєструватися
<?php if (isset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e)): ?>
<?php $component = $__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e; ?>
<?php unset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>


<?php if (isset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d)): ?>
<?php $component = $__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d; ?>
<?php unset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH D:\treble\mentor\OSPanel\domains\ProJectSchool\resources\views/emails/registration.blade.php ENDPATH**/ ?>