
<div class="loginup"  id="hid2">
<div id="hid" style="min-height: 100%;min-width:100%;z-index:200001;position:absolute;">

</div>
<div class="container2"  style=" padding-right: 0;
            padding-left: 0;
            ">
                    <div class="apply-admission bg-apply-type2">
                        <div class="apply-admission-wrap type1 bd-type2">
                            <div class="apply-admission-inner">
                                <h2 class="title text-center">
                                    <span>Увійти</span>
                                </h2>

                            </div>
                        </div>
                        <div class="form-apply">
                            <div class="section-overlay183251"></div>
                            <form  method="POST" action="<?php echo e(route('login')); ?>" class="apply-now">
                                <?php echo csrf_field(); ?>
                                <ul>
                                    <li>
                                    <label for="email" style="color: white;" class="col-md-4 col-form-label "><?php echo e(__('E-Mail Адресса')); ?></label><input id="email" type="email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>

<?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
    <span class="invalid-feedback" role="alert">
        <strong><?php echo e($message); ?></strong>
    </span>
<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?></li>
<li>
<label for="password" style="color: white;" class="col-md-4 col-form-label "><?php echo e(('Пароль')); ?></label>
    <input id="password" type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password"  style="color:aliceblue;" required autocomplete="current-password">

<?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
    <span class="invalid-feedback" role="alert">
        <strong><?php echo e($message); ?></strong>
    </span>
<?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?></li>
                                </ul>
                                <div class="btn-50 hv-border text-center">
                                    <button type="submit"  class="btn bg-clfbb545">
                                       Увійти
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
</Div>
<script>
  var something = document.getElementById('hid');
something.onclick = function() {
 $("#hid2").hide(500);
};
                </script>
<?php /**PATH D:\OpesServer\OSPanel\domains\mentor\resources\views/login.blade.php ENDPATH**/ ?>