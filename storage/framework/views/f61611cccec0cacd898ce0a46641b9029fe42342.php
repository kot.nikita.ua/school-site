
<?php $__env->startSection('bread'); ?>
<li><a href="/about-schools">Школи</a></li>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('name'); ?>
Школи
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="fullsc">
<div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <img src="<?php echo e(asset('storage/')); ?>/<?php echo e($school->image); ?>" alt="images">
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href=""><?php echo e($school->title); ?></a>
                                </div>
                                <p>
                                    <?php echo e($school->excerpt); ?>

                                    </p>
                                
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix">
                                <li class="item-title  overview">
                                    <span class="inner">Про школу</span>
                                </li>
                                
                               
                                
                            </ul>
                            <div class="tab-content-wrap">
                                
                                <div class="tab-content">
                                    <div class="item-content">
                                       <?echo $school->body;?>

                                       
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-right">
                    <form action="<?php echo e(route('update-school')); ?>" class="search-form" method="POST">
                    <?php echo csrf_field(); ?>
                    <div class="price-wrap price-course-single">
                                    
                            

                                       
                    <div class="btn-buynow" style="float: none; width:100%; display: flex;
  align-items: center;
  justify-content: center; ">
                                           <a href="/admin/pages/<?php echo e($school->id); ?>/edit">Редагувати </a> 
                                         
                                        </div>
                                        <div class="btn-buynow" style="float: none; width:100%; display: flex;
  align-items: center;
  justify-content: center; ">
                                           <button type="submit" style="margin:20px">Зберегти</button>
                                          <input type="number" name="school" value="<?php echo e($school->schoolNum); ?>" hidden>
                                        </div>
                                    </div>
                        <!-- сайт школы -->
                        <div class="widget widget-class-start">
                            <div class="widget-title">
                                Сайт школи
                            </div>
                            <div class="content" >
                                    <div class="price-wrap price-course-single">
                                    
                            

                                        <div class="price" style="float: none; width:100%; display: flex;
  align-items: center;
  justify-content: center">
                                          
                                            <input type="text" placeholder="Посилання на сайт школи" value="<?php echo e($school->schoolUrl); ?>" name="school_url" >
                                        </div>
                                        
                                        <div class="btn-buynow" style="float: none; width:100%; display: flex;
  align-items: center;
  justify-content: center">
                                            <a href="" style="padding: 10px 35% 10px 35%;">Перейти</a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <!-- конец сайта -->
                        <div class="widget widget-features">
                            <div class="widget-title">
                               Характеристика закладу
                            </div>
                            <div class="content">
                                <ul class="features1">
                                    <li>
                                        <a href="">№ закладу</a>
                                        <span><?php echo e($school->schoolNum); ?></span>
                                    </li>
                                    <li>
                                        <a href="">Тип закладу</a>
                                        <div id="schoolchoise">
                                        
                                    
                                       
   
   <select name="type" id="cars">
          
       
               <option value="0" <?if ($school->schoolType == 0) {?>selected<?} ?>> Школа</option>
               <option value="1" <?if ($school->schoolType == 1) {?>selected<?} ?>>  Гімназія </option>
               <option value="2" <?if ($school->schoolType == 2) {?>selected<?} ?>> Ліцей </option>
     
     
   </select>    </div>
                                    </li>
                                    <li>
                                        <a href="">Спеціалізація</a>
                                        <input type="text" name="spec" placeholder="Спеціалізація школи (Якщо є) " value="<?php echo e($school->schoolSpec); ?>">
                                    </li>
                                    <li>
                                        <a href="">Ступені навчання</a>
                                        <div id="schoolchoise2">
                                        
                                    
                                       
   
   <select name="styp" id="cars">
          
       
               <option value="0" <?if ($school->schoolStep == 0) {?>selected<?} ?>> 1 - 3</option>
               <option value="1" <?if ($school->schoolStep == 1) {?>selected<?} ?>>  1 - 2 </option>
               <option value="2" <?if ($school->schoolStep == 2) {?>selected<?} ?>> 1 </option>
     
     
   </select>    </div>
                                    </li>
                                    
                                    
                                    
                                </ul>
                                
                            </div>
                        </div>
                        <div class="widget widget-class-start">
                            <div class="widget-title">
                                Контакти
                            </div>
                            <div class="content" >
                            <div class="price-course-single">
                                            
                                                
                                                <h3 style="font-size: large;color:black">Директор:</h3>
                                            
                                                
                                                <p style="padding-left: 5%;"><input type="text" name="direct" placeholder="І'мя директора" value="<?php echo e($school->schoolDirect); ?>"></p>
                                            
                                                
                                                <h3 style="font-size: large;color:black">Телефон:</h3>
                                          
                                                
                                                <p style="padding-left: 5%;"><input type="tel" name="phone" placeholder="Телефон школи" value="<?php echo e($school->schoolPhone); ?>"></p>
                                           
                            </div>            
                            </div>
                        </div>

                        <div class="widget widget-class-start">
                            <div class="widget-title">
                                Школа на карті
                            </div>
                            <div class="content" >
                                    <div>
                                    <div class="price" style="float: none; width:100%; display: flex;
  align-items: center;
  justify-content: center">
                                          
                                            <input type="text" placeholder="Координати школи" value="<?php echo e($school->schoolMap); ?>" name="school_map" >
                                            
                                        </div>
                                        <a href="http://www.openstreetmap.org/">знайти координати школи</a>
                                    <div id="map"style=" height: 400px;  border-radius: 3px;">
										</div>
                                    </div>
                                    <script>
                                    var map = L.map('map').setView([<?php echo e($school->schoolMap); ?>], 17);

                                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                            }).addTo(map);

                                            L.marker([<?php echo e($school->schoolMap); ?>]).addTo(map)
                                    .bindPopup("<?php echo e($school->title); ?>")
                                    .openPopup();
                                    </script>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/about-school-redact.blade.php ENDPATH**/ ?>