<!-- Bootstrap-->
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/bootstrap.css')); ?>">
<!-- Template Style-->
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/font-awesome.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/animate.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/style.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/form.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/shortcodes.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/jquery-fancybox.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/responsive.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/flexslider.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/owl.theme.default.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/owl.carousel.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('assets/stylesheet/jquery.mCustomScrollbar.min.css')); ?>">

<!-- Favicon-->
<link href="<?php echo e(asset('assets/images/logo/favicon.ico')); ?>" rel="shortcut icon">
<?php /**PATH D:\OpesServer\OSPanel\domains\mentor\resources\views/layouts/header-links.blade.php ENDPATH**/ ?>