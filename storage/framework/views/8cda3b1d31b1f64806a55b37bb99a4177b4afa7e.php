<?php $__env->startComponent('mail::message'); ?>
# Новий відгук

До вас надійшов новий відгук з сайта Mentor!

<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr style="background-color: #f8f8f8;">
<td style='padding: 10px; border: #e9e9e9 1px solid; text-align: center;'><b><?php echo e($key); ?></b></td>
<td style='padding: 10px; border: #e9e9e9 1px solid; text-align: center;'><?php echo e($value); ?></td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__env->startComponent('mail::button', ['url' => route('home')]); ?>
    На головну
<?php if (isset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e)): ?>
<?php $component = $__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e; ?>
<?php unset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

З повагою,<br>
<?php echo e(config('app.name')); ?>

<?php if (isset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d)): ?>
<?php $component = $__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d; ?>
<?php unset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH D:\OpesServer\OSPanel\domains\mentor\resources\views/mail/contactus.blade.php ENDPATH**/ ?>