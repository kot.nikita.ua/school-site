<div class="widget widget-latest-post">
    
<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (Auth::check()) {
    $user = Auth::user();
       if ($user->subcribes == 'all') {
            $schools2 = DB::select('SELECT school FROM `posts` WHERE school != 0 ');
            $school2[] = 0;
       foreach( $schools2 as $value2){
           
            $school2[] = $value2->school;
            $school2 = array_unique($school2);
           
            
       }; 
       $school2 = implode(',',$school2);
       }
       
       else{
           $school2 = "0,$user->subcribes";
       }
      
    $lastnews = DB::select("SELECT  title ,updated_at , image , id , school  FROM `posts` WHERE `school` IN ($school2)
     ORDER BY updated_at DESC LIMIT 4");
     
}
else {
    $lastnews = DB::select("SELECT  title ,updated_at , image , id ,school   FROM `posts` WHERE school = 0
    ORDER BY updated_at DESC LIMIT 4");
}
?>
                        <h4 class="widget-title">
                            <span>Останні Новини</span>
                        </h4>
                        <div class="latest-post">
                            <ul class="latest-post-wrap">
                                <?php $__empty_1 = true; $__currentLoopData = $lastnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $onenews): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <!---------------------------------- новая новость -->
                                <li>
                                    <div class="thumb-new" style="margin: 5px;">
                                        <img src="<?php echo e(asset('storage/')); ?>/<?php echo e($onenews->image); ?>" style="width: 80px; height: 80px" alt="images">
                                    </div>
                                    <div class="thumb-new-content clearfix">
                                        <h6 class="thumb-new-title">
                                            <? if($onenews->school == 0){ ?>
                                            <a href="/news/<?php echo e($onenews->id); ?>">
                                                <?php echo e($onenews->title); ?>

                                            </a>
                                            <? }else{ ?>
                                            <a href="/school-news/<?php echo e($onenews->id); ?>">
                                                <?php echo e($onenews->title); ?>

                                            </a>
                                            <?}?>
                                        </h6>
                                        <p class="thumb-new-day">
                                        <?php echo e(date('j/n/Y', strtotime($onenews->updated_at))); ?>

                                        </p>
                                    </div>
                                </li>
                                <!----------------------------------конец новой новости -->
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <li> <p>Немае останніх новин</p> </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div><?php /**PATH D:\OpesServer\OSPanel\domains\mentor\resources\views/lastnews.blade.php ENDPATH**/ ?>