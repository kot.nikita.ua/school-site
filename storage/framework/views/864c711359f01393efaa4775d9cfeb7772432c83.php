<div id="loading-overlay">
    <div class="loader"></div>
</div>
<div class="wrap-header">
    <div class="flat-header flat-header-style2" >
    <div class="top-bar clearfix">
            <div class="container" >
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                    </div>
                </div>
            </div>
        </div>
        <header class="header menu-bar hv-menu-type2">
            <div class="container">
                <div class="menu-bar-wrap clearfix">
                    <div id="logo" class="logo">
                        <a href="<?php echo e(route('home')); ?>"><img src="<?php echo e(asset('assets/images/logo/02.png')); ?>" alt="mentor" style="width: 170px; height: 35px"></a>
                    </div>
                    <div class="mobile-button"><span></span></div>
                    <div class="header-menu">
                        <nav id="main-nav" class="main-nav">
                            <ul class="menu">
                                <li class="menu-item"><a href="<?php echo e(route('home')); ?>">Головна</a></li>
                                <li class="menu-item"><a href="<?php echo e(route('news')); ?>">Новини</a></li>
                                <li class="menu-item"><a href="<?php echo e(route('about-schools')); ?>">Школи</a></li>
                                <?php if(Auth::check()): ?>
                                    <li class="menu-item"><a href="<?php echo e(route('school-news')); ?>">Шкільні новини</a></li>
                                <?php endif; ?>
                                <li class="menu-item"><a href="<?php echo e(route('about')); ?>">Про проект</a></li>
                                <?php if(Route::has('login')): ?>
                        <?php if(auth()->guard()->check()): ?>
                        <li><a href=""><?php echo e($user->name); ?> <img id="avatar" src="<?php echo e(asset('storage/')); ?>\<?php echo e($user->avatar); ?>" alt="" style="width: 30px; height :30px;border-radius: 30px 30px 30px 30px; "></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="/user/<?php echo e(Auth::id()); ?>">Налаштування</a></li>
                                            <?php if($user->role_id == 1): ?>
                                            <li class="menu-item "><a href="/admin">Адмін панель</a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 4 Or $user->role_id == 5 Or $user->role_id == 1): ?>
                                            <li class="menu-item "><a href="/user/upgrade">Заявки</a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 4 ): ?>
                                            <li class="menu-item "><a href="/about-schools/redact">Опис Школи</a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 4 Or $user->role_id == 5 Or $user->role_id == 1): ?>
                                            <li class="menu-item "><a href="/upgradedusers">Список <?php if($user->role_id == 4): ?>вчителів <?php endif; ?> <?php if($user->role_id == 5): ?>адміністраторів <?php endif; ?> <?php if($user->role_id == 1): ?>глобальних адміністраторів <?php endif; ?></a></li>
                                            <?php endif; ?>
                                            <?php if($user->role_id == 3 OR $user->role_id == 4 Or $user->role_id == 5): ?>
                                            <li class="menu-item "><a href="<?php echo e(route('postsview')); ?>">Мої новини</a></li>
                                            <?php endif; ?>
                                            <li class="menu-item "><a href="/logout">Вихід</a></li>
                                        </ul><!-- sub-menu -->


                                    </li>

                    <?php else: ?>
                    <li class="menu-item"><a href="#" id="login">Увійти</a></li>
                      <?php endif; ?>
                      <?php endif; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    </div><!-- header -->
</div><!-- wrap-header -->
<section class="flat-slider style2" style="background-color: rgba(0,0,0,0.4); ">
    <div class="rev_slider_wrapper fullwidthbanner-container" >
        <div id="rev-slider3" class="rev_slider fullwidthabanner">
            <ul>
                <!-- Slide 1 -->
                <li data-transition="random">
                    <!-- Main Image -->
                    <img src="<?php echo e(asset('assets/images/home/01.png')); ?>" alt="Ментор" data-bgposition="center center" data-no-retina>
                    <div class="overlay"></div>
                    <!-- Layers -->
                    <div class="tp-caption tp-resizeme education"
                         data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-161','-100','-80','-50']"
                         data-fontsize="['70','70','70','30']"
                         data-lineheight="['90','90','90','35']"
                         data-width="full"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"> <div class="education-text text-white">Дізнавайся про новини першим</div> </div>
                    <div class="tp-caption tp-resizeme text-white complete text-edukin"
                         data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-70','-10','5','20']"
                         data-fontsize="['17','17','16','14']"
                         data-lineheight="['30','30','26','22']"
                         data-width="full"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-paddingright="['550','155','50','2']" >Реєструйся та дізнавайся про шкільні новини у твоему місті швидше ніж тебе повідомлять у вайбері</div>
                    <div class="tp-caption"
                         data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['40','80','90','90']"
                         data-width="full"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-paddingtop= "['50','50','50','50']"
                         data-paddingbottom= "['50','50','50','50']"> <a href="#how" class="btn btn-styl1">Дізнатися більше</a> </div>
                </li>
                <!-- Slide 2 -->
                <li data-transition="random">
                    <!-- Main Image -->
                    <img src="<?php echo e(asset('assets/images/home/02.png')); ?>" alt="Ментор" data-bgposition="center center" data-no-retina>
                    <div class="overlay"></div>
                    <!-- Layers -->
                    <div class="tp-caption tp-resizeme font-rubik education education"
                         data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-161','-100','-80','-50']"
                         data-fontsize="['70','70','70','30']"
                         data-lineheight="['90','90','90','35']"
                         data-width="full"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"> <div class="education-text text-white">Дізнавайся про новини першим</div> </div>
                    <div class="tp-caption tp-resizeme text-white complete text-edukin"
                         data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-70','-10','5','20']"
                         data-fontsize="['17','17','16','14']"
                         data-lineheight="['30','30','26','22']"
                         data-width="full"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-paddingright="['550','155','50','2']" >Батьківські збори, медичні довідки, закриття на карантин - про це все ти будеш дізнаватися відразу після оголошення!</div>
                    <div class="tp-caption"
                         data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['40','80','90','90']"
                         data-width="full"
                         data-height="none"
                         data-whitespace="normal"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];"
                         data-mask_out="x:inherit;y:inherit;"
                         data-start="1000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-paddingtop= "['50','50','50','50']"
                         data-paddingbottom= "['50','50','50','50']"> <a href="#how" class="btn btn-styl1">Дізнатися більше</a> </div>
                </li>
            </ul>
        </div>
    </div> <!-- slider -->
</section><!-- flat-slider -->
<?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/layouts/header2.blade.php ENDPATH**/ ?>