
<?php $__env->startSection('content'); ?>

<div class="blog-bl content-blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="site-content">
                    <div class="user">
                        <div class="user_image" style="display: flex;
  align-items: center;
  justify-content: center ">
                            <img src="<?php echo e(asset('storage/')); ?>\<?php echo e($user->avatar); ?>" alt=""
                                style="min-width: 200px;min-height :200px; height:201px;width:201px;border-radius: 200px; background-color:#028174 ">
                        </div>
                        <div class="user_name" style="display: flex;
  align-items: center;
  justify-content: center ">
                            <p><?php echo e($user->name); ?></p>
                            
                        </div>
                        <div class="user_name" style="display: flex;
  align-items: center;
  justify-content: center ; margin-top:5px">
                        <?php if($user->role_id == 1): ?>
                            <p>Создатель</p>
                        <?php elseif($user->role_id == 2): ?> 
                            <p>Користувач</p>
                        <?php elseif($user->role_id == 3): ?> 
                            <p>Вчитель Школи №:<?php echo e($user->school); ?></p>
                        <?php elseif($user->role_id == 4): ?> 
                            <p>Адміністратор Школи №: <?php echo e($user->school); ?></p>
                        <?php elseif($user->role_id == 5): ?> 
                            <p>Глобальный Адміністратор</p>
                        <?php else: ?>
                            <p>Гість</p>
                        <?php endif; ?> 
                        </div>
                    </div>

                    <form enctype="multipart/form-data" action="<?php echo e(route('updatepic')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <div style="display: flex;
  align-items: center;
  justify-content: center ">


                            <div class="changepic">

                                <ul>


                                    <li><input name="image" type="file" required/></li>
                                </ul>

                            </div>
                        </div>
                        
                        <div style="display: flex;
  align-items: center;
  justify-content: center; margin-bottom:20px ">
                            <input type="submit" value="Змінити аватар" />
                        </div>

                    </form>
                    <?php if($req === false): ?>
                    <?php if($user->role_id == 2 Or $user->role_id == 3 Or $user->role_id == 4): ?>
                    <div style="display: flex;
  align-items: center;
  justify-content: center ">
                    <button id="change">Подати заявку на підвищення</button>
</div>
                    <div class="container" style="margin-bottom: 20px;" id="upgrade">
                        
                    
                    <div class="widget widget-search" style="margin-top: 20px;">
                        <div class="search-blog-wrap">
                        <p style="color: black; font-weight:500; font-size:larger">Подати заявку на підвищення</p>
                            <form action="<?php echo e(route('send')); ?>" class="search-form" method="POST">
                            <?php echo csrf_field(); ?>
                            <label for="role">Виберіть посаду:</label>

<select name="role" id="role" onchange="hid(this.value)">
                        <?php if($user->role_id == 2): ?>
                          <option value="teacher" id="teacher">Шкільний Вчитель</option> 
                            <option value="admin" id = "admin">Шкільний Адміністратор</option>
                            <option value="globaladmin" id = "globaladmin">Глобальный Адміністратор</option> 
                        <?php elseif($user->role_id == 3): ?> 
                            <option value="admin" id = "admin">Шкільний Адміністратор</option>
                            <option value="globaladmin" id = "globaladmin">Глобальный Адміністратор</option> 
                        <?php elseif($user->role_id == 4): ?> 
                        <option value="globaladmin" id = "globaladmin">Глобальный Адміністратор</option> 
                        <?php endif; ?>

</select>            
                                <input type="tel" placeholder="Ваш телефон" name="phone" style="margin-top: 5px;"
                                     required>
                                     <?php if($user->role_id != 4): ?>
                                     <div id="schoolchoise">
                                        
                                    
                                     <label for="cars">Виберіть школу:</label>

<select name="school" id="cars">
       
    <?php
        for ($i=1; $i < 27; $i++) { 
            ?> <option value="<?php echo e($i); ?>"> <p id="school"> Школа №:<span> <?php echo e($i); ?> </span></p></option><?
        }
    ?>
   <option value="0" id = "globalAdminOption" hidden>Глобальный Адміністратор</option> 
  
</select>    </div>
<?php endif; ?>         



                                <input type="submit" value="Відіслати"id="send" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>
                </div>
                   <?php endif; ?>
                   <?php else: ?>
                   
                   <div style="padding:20px; box-shadow: 0px 0px 20px 8px rgba(208, 208, 208, 0.2);margin-bottom:20px">
                       
                   
                   <div class="flat-course clearfix">
                        <div class="featured-post">
                            <div class="entry-image" style="margin-left: 20px;">
                                <img src="<?php echo e(asset('storage/')); ?>\<?php echo e($upgrade->avatar); ?>" alt="images" style="height:100px;width:100px; border-radius:100px">
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                   
                                    <a href="#">Заява на підвищення відправленна перевірте електронну пошту</a>
                                   
                                </h4>
                                <p>
                                    Підвищення до : <?php if($upgrade->role == 'teacher'): ?> Вчителя Школи №:<?php echo e($upgrade->school); ?> <?php elseif($upgrade->role == 'admin'): ?>Адміністратор Школи №:<?php echo e($upgrade->school); ?><?php elseif($upgrade->role == 'globaladmin'): ?> Глобальный Адміністратор <?php endif; ?>
                                </p>
                                <div class="author-info">
                                    <div class="author-name">
                                        <?php echo e($upgrade->name); ?>

                                    </div>
                                   
                                </div>
                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">
                                        <form method="POST" action="<?php echo e(route('selfdeletereq')); ?>">
                                            <?php echo csrf_field(); ?>
                                            <button class="delete-post" type="submit" name="post" value="<?php echo e($upgrade->id); ?>" >Видалити Заяву</button>
                                        </form>
                                        
                                    </div>
                                    <div class="price">
                                    <span class="price-now"><?php echo e(date('j/n/Y', strtotime($upgrade->date))); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   <?php endif; ?> 
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
               
                    <div class="widget widget-search" style="margin-top: 20px;">
                        <div class="search-blog-wrap">
                        <p style="color: black; font-weight:500; font-size:larger">Змінити Ім'я та Email</p>
                            <form action="<?php echo e(route('updateuser')); ?>" class="search-form" method="POST">
                            <?php echo csrf_field(); ?>
                                <input type="email" placeholder="Email" name="email" style="margin-top: 5px;"
                                    value="<?php echo e($user->email); ?>" required>
                                <input type="text"  placeholder="Ім'я" name="name" style="margin-top: 5px;" value="<?php echo e($user->name); ?>"
                                    required>



                                <input type="submit" value="Зберегти" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>

                    <div class="widget widget-search">
                        <div class="search-blog-wrap">
                            <p style="color: black; font-weight:500; font-size:larger">Змінити підписки на школи</p>
                            <form action="<?php echo e(route('updatesub')); ?>" class="search-form" method="POST">
                            <?php echo csrf_field(); ?>
                                <ul>
                                    <? 
                                            foreach($schools as $school){
                                                    $school = explode(',',$school);
                                                if ($school[0] == 'all') {
                                                  
                                               
                                                    if (isset($school[1])) {
                                                        ?>
                                                        <li><input type="checkbox" id="all" onclick="uncheckall()" placeholder="Email" name="option2[]" value="all" checked>
                                        <label for="subscribeNews">
                                            <p id="school"> Всі Школи </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                           <li><input type="checkbox" id="all" onclick="uncheckall()" placeholder="Email" name="option2[]" value="all" >
                                        <label for="subscribeNews">
                                            <p id="school"> Всі Школи </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }else {
                                                    if (isset($school[1])) {
                                                        ?>
                                                        <li><input type="checkbox" placeholder="Email" id="one" onclick="uncheckone()" name="option2[]" value="<?php echo e($school[0]); ?>" checked>
                                        <label for="subscribeNews">
                                            <p id="school"> Школа №:<span> <?php echo e($school[0]); ?> </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                          <li><input type="checkbox" placeholder="Email" id="one" onclick="uncheckone()" name="option2[]" value="<?php echo e($school[0]); ?>" >
                                        <label for="subscribeNews">
                                            <p id="school"> Школа №:<span> <?php echo e($school[0]); ?> </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }

                                             
                                            }
                                        ?>
                                    
                                           
                                            
                                    
                                            
                                        
                                </ul>


                                <input type="submit" value="Зберегти" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>
                    <div class="widget widget-search">
                        <div class="search-blog-wrap">
                            <p style="color: black; font-weight:500; font-size:larger">Змінити підписки на класси</p>
                            <form action="<?php echo e(route('updatesubclass')); ?>" class="search-form" method="POST">
                            <?php echo csrf_field(); ?>
                                <ul>
                                    <? 
                                   
                                            foreach($classsub as $class){
                                                    $class = explode(',',$class);
                                                if ($class[0] == 'all') {
                                                    
                                               
                                                    if (isset($class[1])) {
                                                        ?>
                                                        <li><input type="checkbox" id="all1" onclick="uncheckall2()" placeholder="Email" name="option2[]" value="all" checked>
                                        <label for="subscribeNews">
                                            <p id="class"> Всі класси </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                           <li><input type="checkbox" id="all1" onclick="uncheckall2()" placeholder="Email" name="option2[]" value="all" >
                                        <label for="subscribeNews">
                                            <p id="class"> Всі класси </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }else {
                                                    if (isset($class[1])) {
                                                        ?>
                                                        <li><input type="checkbox" placeholder="Email" id="one1" onclick="uncheckone2()" name="option2[]" value="<?php echo e($class[0]); ?>" checked>
                                        <label for="subscribeNews">
                                            <p id="class"> Класс №:<span> <?php echo e($class[0]); ?> </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                          <li><input type="checkbox" placeholder="Email" id="one1" onclick="uncheckone2()" name="option2[]" value="<?php echo e($class[0]); ?>" >
                                        <label for="subscribeNews">
                                            <p id="class"> Класс №:<span> <?php echo e($class[0]); ?> </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }

                                             
                                            }
                                        ?>
                                    
                                           
                                            
                                    
                                            
                                        
                                </ul>


                                <input type="submit" value="Зберегти" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>                        
                   
                        </div>
 
            </div>
        </div>
    </div>
</div><!-- content-blog -->
<script>
    function hid(role){
        if (role === "globaladmin") {
           $('#schoolchoise').hide();
        }
        else{
            $('#schoolchoise').show();
        }
        
    }
    
    function uncheckall(){
        var checkBox = document.getElementById("all");
        if (checkBox.checked == true) {
            uncheckElements();
            checkBox.checked=true;
        }
    }
    function uncheckone(){
        
        
       
            var checkBox2 = document.getElementById("all"); 
            checkBox2.checked=false;
       
    }
    function uncheckall2(){
        var checkBox = document.getElementById("all1");
        if (checkBox.checked == true) {
            uncheckElements1();
            checkBox.checked=true;
        }
    }
    function uncheckone2(){
        
        
       
            var checkBox2 = document.getElementById("all1"); 
            checkBox2.checked=false;
       
    }
    function uncheckElements()
{
 var uncheck=document.getElementsByTagName('input');
 for(var i=0;i<uncheck.length;i++)
 {
  if(uncheck[i].type=='checkbox')
  {
    if(uncheck[i].id=='one'){
        uncheck[i].checked=false;
    }
  }
 }
}
function uncheckElements1()
{
 var uncheck=document.getElementsByTagName('input');
 for(var i=0;i<uncheck.length;i++)
 {
  if(uncheck[i].type=='checkbox')
  {
    if(uncheck[i].id=='one1'){
        uncheck[i].checked=false;
    }
  }
 }
}
</script>
<?php if(isset($path)): ?>
<img src="<?php echo e(asset('/storage/' . $path)); ?>" alt="">
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\OpenServer\domains\ProJectSchool\resources\views/user.blade.php ENDPATH**/ ?>