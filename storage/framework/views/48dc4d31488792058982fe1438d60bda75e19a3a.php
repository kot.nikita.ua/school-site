<?

function simpl($num){
    if ($num > 1000 && $num < 1000000) {
        $num1 = $num % 1000;
        $simpl = $num1."K";
    }elseif($num >1000000){
        $num1 = $num / 1000000;
        $num1 = round($num1 , 2);
        $simpl = $num1."M";
    }else{
        $simpl = $num;
    }
    return $simpl;
}

?>
<div class="flat-courses clearfix isotope-courses" >
<div class="row align-center mt-5 mr-1 ml-1" >
                <?php $__empty_1 = true; $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?> 
                <div class="course clearfix Marketing Certificate Popular" >
               
                    <div class="flat-course" >
                        <div class="featured-post post-media">
                            <div class="entry-image pic">
                                <img src="storage/<?php echo e($content->image); ?>" alt="images">
                                <div class="hover-effect"></div>
                                <div class="links">
                                    <a href="/news/<?php echo e($content->id); ?>">Детальніше</a>
                                </div>
                            </div>
                        </div>
                        <div class="course-content clearfix" >
                            <div class="wrap-course-content" >
                                <h4>
                                    <a href="/news/<?php echo e($content->id); ?>"><?php echo e($content->title); ?></a>
                                </h4>
                                <p>
                                    <?php echo e($content->excerpt); ?>

                                </p>

                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">

                                        <span><?php echo e($content->name); ?></span>
                                    </div>
                                    <div class="price">
                                    <span class="price-previou">
                                            <img src="<?php echo e(asset('assets/icons/vision.png')); ?>" style="height: 25px; width:25px; margin-bottom:1px; opacity:0.5;"><span>:<?echo simpl($content->views) ?></span>
                                        </span>|
                                        <span class="price-now"><?php echo e(date('j/n/Y', strtotime($content->updated_at))); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
        <p>Нічого не знайдено</p>
    </div>
    <?php endif; ?>

</div>
</div> 
<?php if($news->lastPage() !== 1): ?>
<div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
    <button class="page-numbers current" id="addBlog" style="margin:20px">Загрузити більше</button></li>
</div>
<script>
var count = document.getElementById('addBlog');
count.onclick = function() {
        var text = $('#reqest').val();
       var value = Number("12");
       counter += value;
         
     search(text,counter,sort);
 };
</script>

<?php endif; ?>          
            <?php /**PATH D:\treble\mentor\OSPanel\domains\ProJectSchool\resources\views/news-ajax.blade.php ENDPATH**/ ?>