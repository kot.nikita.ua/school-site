<div class="site-content">
    <!-- класна новина -->
    <?php $__empty_1 = true; $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <article class="post-blog box-shadow-type2">
        <div class="featured-post">
            <img src="storage/<?php echo e($content->image); ?>" alt="images">
        </div>
        <div class="content-post content-post-blog">
            <div class="post-meta">
                <div class="clendar-wrap">
                    <div class="day">
                        <?php echo e(date('j', strtotime($content->updated_at))); ?>

                    </div>
                    <div class="month">
                        <?php echo e(date('F', strtotime($content->updated_at))); ?>

                    </div>
                    <div class="day" style="margin-top: 5px;">
                        <?php echo e(date('Y', strtotime($content->updated_at))); ?>

                    </div>
                </div>

            </div>
            <div class="content-post-inner">
                <div class="poster lt-sp0029">
                    Виклав :<span><a href="#"><?php echo e($content->name); ?></a></span>
                </div>

                <h3 class="entry-title">
                    <a href="/school-news/<?php echo e($content->id); ?>" class="lt-sp0023"><?php echo e($content->title); ?></a>
                </h3>
                <p>
                    <?php echo e($content->excerpt); ?></p>
                <div class="flat-button">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="/school-news/<?php echo e($content->id); ?>">Читати Далі</a>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-3">
                            <p id="school"> Школа №:<span> <?php echo e($content->school); ?> </span></p>
                        </div>
                        <div class="col-md-3">
                            <p id="class"> Клас: <span> <?php echo e($content->class); ?> </span></p>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </article>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
        <p>Нічого не знайдено</p>
    </div>
    <?php endif; ?>
    <!-- конец класной новине -->

</div>
<?php if($news->lastPage() !== 1): ?>
<div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
    <button class="page-numbers current" id="addBlog" style="margin:20px">Загрузити більше</button></li>
</div>
<script>
var count = document.getElementById('addBlog');
count.onclick = function() {
    var text = $('#search').val();
    var value = Number("1");
    counter += value;
    
    search(text, counter, school3, clas3);
};
</script>
<?php endif; ?><?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/school-news-ajax.blade.php ENDPATH**/ ?>