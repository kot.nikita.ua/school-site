
<?php $__env->startSection('bread'); ?>
<li><a href="/about-schools">Школи</a></li>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('name'); ?>
Школи
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="fullsc">
<div class="blog-bl content-blog" style="margin-bottom: 40px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-content">
                    <?php $__empty_1 = true; $__currentLoopData = $schools; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $school): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <article class="post-blog box-shadow-type2">
                            <div class="featured-post">
                                <img src="<?php echo e(asset('storage/')); ?>/<?php echo e($school->image); ?>" alt="images" style="width: 100%;">
                            </div>
                            <div class="content-post content-post-blog">
                                <div class="post-meta">
                                    <div class="clendar-wrap">
                                        <div class="day">
                                        <?php echo e($school->schoolNum); ?>

                                        </div>
                                        <div class="month">
                                          №
                                        </div>
                                    </div>
                                    <ul class="social" hidden> 
                                        <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                        
                                        <li><i class="fa fa-instagram" aria-hidden="true"></i></li>
                                        
                                    </ul>
                                </div>
                                <div class="content-post-inner">
                                   
                                    <h3 class="entry-title">
                                        <a href="/about-schools/<?php echo e($school->id); ?>" class="lt-sp0023"><?php echo e($school->title); ?></a>
                                    </h3>
                                    <p>
                                    <?php echo e($school->excerpt); ?>

                                    </p>
                                    <div class="flat-button">
                                        <a href="/about-schools/<?php echo e($school->id); ?>">Дізнатися більше</a>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <p>Нема Заяв</p>
                    <?php endif; ?>
                    </div>
                    <?php echo e($schools->links('layouts.pagination')); ?>

                </div>
               
            </div>
        </div>
    </div><!-- content-blog -->
</div> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\OpenServer\domains\school-site\resources\views/about-school.blade.php ENDPATH**/ ?>