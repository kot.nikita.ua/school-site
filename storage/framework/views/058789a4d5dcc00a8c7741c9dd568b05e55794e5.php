
<?php $__env->startSection('content'); ?>
<div class="page-title page-title-blog">
    <div class="page-title-inner">
        <div class="breadcrumbs breadcrumbs-blog text-left">
            <div class="container">
                <div class="breadcrumbs-wrap">
                    <ul class="breadcrumbs-inner">
                        <li><a href="index.html">Головна</a></li>
                        <li><a href="course.html">Новини</a></li>
                    </ul>
                    <div class="title">
                        Новини
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<!-- bg-header -->

<div class="course-grid">
    <div class="container">
        <div class="flat-portfolio">
            <ul class="flat-filter-isotype">
                <li>
                    <div style="color: #333333;">
                        <p>Сортувати :</p>
                    </div>
                </li>
                <li class="active">
                    <a href="#" id="date">За датою</a>
                </li>
                <li>
                    <a href="#" id="views">За Популярністю</a>
                </li>
            </ul>
            <div class="search-course">
                <div class="search-form">
                    <input type="search" placeholder="Пошук" id="reqest">
                    <button class="search-button" type="button" id="sear">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>






        <div id="refresh">
            <?php echo $__env->make('news-ajax', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>









    </div>
</div>

<script>
var sear = document.getElementById('date');
var sear = document.getElementById('views');

var counter = Number("12");
var sort = 'date';
var sear = document.getElementById('sear');
sear.onclick = function() {
    var text = $('#reqest').val();

    search(text, counter, sort);

};

date.onclick = function() {
    var text = $('#reqest').val();
    var sort = 'date';

    search(text, counter, sort);

};
views.onclick = function() {
    var text = $('#reqest').val();
    var sort = 'views';

    search(text, counter, sort);

};



function search(search, counter, sort) {

    $.ajax({

            url: "<?php echo e(route('newsSearch')); ?>",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                Search: search,
                addBlog: counter,
                Sort: sort
            }
        })
        .done(

            function(data) {


                $('#refresh').hide().html(data).fadeIn('slower');
            });
}
</script>
<!-- course-grid -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\treble\mentor\OSPanel\domains\ProJectSchool\resources\views/news.blade.php ENDPATH**/ ?>