<?php

use App\Http\Controllers\GlobalNews;
use App\Mail\registration;
use App\Mail\upgradeAccept;
use App\Mail\upgradeDeny;
use App\Mail\upgradeRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
//TESTAREA
Route::get('/reqmail', function () {

    return new upgradeAccept();
});

//HOME
Route::get('/', 'Home@home')->name('home');
Route::post('/', 'Registration@email')->name('home');

//USER
Route::get('/user/upgrade', 'User@upgrade')->name('upgrade')->middleware('auth');
Route::get('/user/{id}', 'User@settings')->name('user')->middleware('auth');
Route::post('/user/update', 'User@update')->name('updatepic')->middleware('auth');
Route::post('/user/send', 'User@send')->name('send')->middleware('auth');
Route::post('/user/delete', 'User@delete')->name('deletereq')->middleware('auth');
Route::post('/user/selfdelete', 'User@selfdelete')->name('selfdeletereq')->middleware('auth');
Route::post('/user/aprove', 'User@aprove')->name('aprovereq')->middleware('auth');
Route::get('/userposts', 'Postcreation@view')->name('postsview');
Route::get('/upgradedusers', 'User@seeteachers')->name('teacherview');
Route::post('/upgradedusers/downgrade', 'User@downgradeteacher')->name('teacherdowngrade');
Route::post('/userposts/delete', 'Postcreation@delete')->name('deletepost')->middleware('auth');
Route::post('/user/updateuser', 'User@updateuser')->name('updateuser')->middleware('auth');;
Route::post('/user/updatesub', 'User@updatesub')->name('updatesub')->middleware('auth');
Route::post('/user/updatesubclass', 'User@updatesubclass')->name('updatesubclass')->middleware('auth');
Route::get('/about-schools/redact', 'Sckools@redact')->name('redact-school')->middleware('auth');
Route::post('/about-schools/update', 'Sckools@update')->name('update-school')->middleware('auth');
Route::post('/addschool', 'User@addschool')->name('addschool')->middleware('auth');
Route::post('/deleteschool', 'User@deleteschool')->name('deleteschool')->middleware('auth');
//COMMENTS
Route::post('/news', 'Ajax@globalsearch')->name('newsSearch');
Route::post('/add', 'Ajax@addcoment')->name('addcoment')->middleware('auth');
Route::post('/update', 'Ajax@refreshcomments')->name('update')->middleware('auth');
Route::post('/delete', 'Ajax@delete')->name('delete')->middleware('auth');
//GLOBAL NEWS
Route::get('/news/{id}', 'GlobalNews@single')->name('news-single');
Route::get('/news', 'GlobalNews@all')->name('news');
//LOCAL NEWS
Route::get('/school-news', 'SchoolNews@all')->name('school-news')->middleware('auth');
Route::post('/school-news', 'Ajax@Schoolsearch')->name('schoolnewsSearch')->middleware('auth');
Route::get('/school-news/{Id}', 'SchoolNews@single')->name('school-news-single')->middleware('auth');
//ABOUT
Route::get('/about', 'Home@about')->name('about');
Route::post('/contact-us', 'ContactUsContcroller@main')->name('contact-us');
//ABOUT SCHOOLS
Route::get('/about-schools', 'Sckools@schoollist')->name('about-schools');
Route::get('/about-schools/{id}', 'Sckools@schoolone');

//ELSE
Route::redirect('/home', '/');
Route::get('/registration/{token}', 'Registration@reg')->name('reg');
/* Authorization routes section start*/
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');
/*Route::get('/home', 'HomeController@index')->name('home');*/
/* Authorization routes section end */

/* Admin panel routes section start */
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
/* Admin panel routes section end */
