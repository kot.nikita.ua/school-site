<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class upgradeRequire extends Mailable
{
    use Queueable, SerializesModels;
    private $name = 'default name';
    private $phone = 0000000000;
    private $state = 'default role' ;
    private $email = 'default email' ;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$phone,$state,$email)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->state = $state;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->name;
        $phone = $this->phone;
        $state = $this->state;
        $email = $this->email;
        return $this->markdown('mail.upgradeRequire',['name'=> $name,'phone'=> $phone,'state'=> $state,'email'=> $email]);
    }
}
