<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class Newsmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   
    public function __construct($request)
    {

        $id = DB::table('posts')
  
        ->select('id')
       
        
        ->where([
            ['title', '=', $request->title],
            ['excerpt', '=', $request->excerpt],
            
            ['slug', '=', $request->slug]
        ])
       
       ->orderByDesc('updated_at')->get();
       
        $this->title = $request->title;
        $this->exert = $request->excerpt;
        $this->id = $id[0]->id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->markdown('email.news',['title'=>$this->title,'exert'=>$this->exert,'id'=>$this->id]);
    }
}
