<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class upgradeRequest extends Mailable
{
    use Queueable, SerializesModels;
    private $name = 'default name';
    private $phone = 0000000000;
    private $state = 'default role' ;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$phone,$state)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->state = $state;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->name;
        $phone = $this->phone;
        $state = $this->state;
        return $this->markdown('mail.upgradeReq',['name'=> $name,'phone'=> $phone,'state'=> $state]);
    }
}
