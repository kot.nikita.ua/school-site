<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Downgrade extends Mailable
{
    use Queueable, SerializesModels;
    private $send = 'default role';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($send)
    {
       $this->send = $send;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $send = $this->send;
        return $this->markdown('mail.downgrade',['role'=>$send]);
    }
}
