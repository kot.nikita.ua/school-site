<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ajax extends Controller
{
    public function globalsearch(Request $request)
    {
        if ($request->Sort == 'views') {
            $sort = 'posts.views';
        }else{
            $sort = 'posts.updated_at';
        }

        $text = $request->Search;   
        $news = DB::table('posts')
  
        ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt' ,'posts.views')
        ->join('users', 'posts.author_id', '=', 'users.id')
        
        ->where([
            ['posts.school', 'IN', 0],
            ['posts.title', 'LIKE', "%$text%"],
        ])
       
       ->orderByDesc($sort)
        ->paginate($request->addBlog);



        
        return view('news-ajax',['news'=>$news])->render();
    }
    public function schoolsearch(Request $request){ 
        $schoolsearch = explode(',',$request->School);
        if ($request->Class != 'all') {
           
            $news = DB::table('posts')
  
        ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt','posts.school','posts.class' ,'posts.views')
        ->join('users', 'posts.author_id', '=', 'users.id')
        
        ->whereIn('posts.school',$schoolsearch)
       ->where([
        ['posts.class', '=', $request->Class],
        ['posts.title', 'LIKE', "%$request->Search%"],
    ])
       ->orderByDesc('posts.updated_at')
        ->paginate($request->addBlog);
       
        }
        else{
            $news = DB::table('posts')
  
        ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt','posts.school','posts.class' ,'posts.views')
        ->join('users', 'posts.author_id', '=', 'users.id')
        
        ->whereIn('posts.school',$schoolsearch)
       ->where(
       
        'posts.title', 'LIKE', "%$request->Search%"
    )
       ->orderByDesc('posts.updated_at')
        ->paginate($request->addBlog);
            
        }
        return view('school-news-ajax',['news'=>$news])->render();
    }
    public function addcoment(Request $request){
        
        DB::table('Comments')->insert(
            ['text' => $request->text, 'user_id' => $request->user , 'post_id' => $request->post]
        );
    }
    public function refreshcomments(Request $request){
        $comments =  DB::table('Comments')
  
        ->select('Comments.id' ,'users.name' ,'Comments.text','Comments.created_at' ,'users.avatar','Comments.user_id')
        ->join('users', 'users.id', '=', 'Comments.user_id')
        ->join('posts', 'comments.post_id', '=', 'posts.id')
        ->where('posts.id', '=', $request->id)
       
        ->orderByDesc('Comments.created_at')
        ->paginate(8)->withPath("$request->id");
        
        return view('comments',['comments'=>$comments])->render();
    }
    public function delete(Request $request){
        DB::table('Comments')->where('id', '=', $request->id)->delete();
    }
}
