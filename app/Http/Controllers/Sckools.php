<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class Sckools extends Controller
{
   public function schoollist(){
    $schools = DB::table('pages')->select('*')->paginate(10);

    return view('about-school',['login'=>0,'schools'=>$schools]);
   }
   public function schoolone($id){
    $school = DB::select("SELECT * FROM `pages` WHERE id = $id ");
    return view('about-school-single',['login'=>0,'school'=>$school[0]]);
   }
   public function redact(Request $request){
    $user = Auth::user();
    $school = DB::select("SELECT * FROM `pages` WHERE schoolNum = $user->school ");
    if (!isset($school[0])) {
      
      DB::table('pages')->insert(
        ['title'=>'','author_id' => $user->id ,'SchoolUrl' => '' ,'SchoolType'=>0,'SchoolSpec'=>'','SchoolStep'=> 0,'SchoolNum'=>$user->school,'SchoolMap'=>'','SchoolDirect'=>'','SchoolPhone'=>'']
    );
      $school = DB::select("SELECT * FROM `pages` WHERE schoolNum = $user->school ");
    }
    
    if (isset($request->message)) {
      return view('about-school-redact',['login'=>0,'school'=>$school[0],'message'=>$request->message,]); 
    }else{
      return view('about-school-redact',['login'=>0,'school'=>$school[0]]);  
    }
   }
   public function update(Request $request){
      
      DB::table('pages')
              ->where('schoolNum', $request->school)
              ->update(['SchoolUrl' => $request->school_url ,'SchoolType'=>$request->type,'SchoolSpec'=>$request->spec,'SchoolStep'=>$request->styp,'SchoolMap'=>$request->school_map,'SchoolDirect'=>$request->direct,'SchoolPhone'=>$request->phone]);
              return redirect()->route("redact-school",['message'=>'Данні закладу зміннено']);
   }
   
}
