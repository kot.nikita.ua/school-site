<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SchoolNews extends Controller
{
    public function all(){
       $user = Auth::user();
       
       if ($user->subcribes == 'all') {
            $schools = DB::select('SELECT school FROM `posts` WHERE school != 0 ');
      
       foreach( $schools as $value){
           
            $school[] = $value->school;
            $school = array_unique($school);
           
            $schoolsearch = $school;
            $schoolsub = $school;
       }; 
       $school = implode(',',$school);
       }
       
       else{
           $schools = $user->subcribes;
           $school = $schools;
            $schools = explode(',',$schools);
            $schoolsearch = $schools;
           $schoolsub = $schools;
           }
      
        
        $news = DB::table('posts')
  
        ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt','posts.school','posts.class' ,'posts.views')
        ->join('users', 'posts.author_id', '=', 'users.id')
        
        ->whereIn('posts.school',$schoolsub)
       
       ->orderByDesc('posts.updated_at')
        ->paginate(12);

        $clas = DB::select("SELECT class FROM `posts` WHERE school IN ($school) ");
        foreach($clas as $new){
            
            $classes[] =$new->class;
            $classes = array_unique($classes);
        }
       
        return view('school-news',['news'=>$news , 'login'=>0,'schoolall' =>$school, 'search'=>$schoolsearch ,'classSearch'=>$classes]);


    }
    public function single($id){
       
        $news = DB::select('SELECT  u.name , n.title ,n.updated_at,n.image ,n.id ,n.body,n.views,u.avatar,n.school,n.class FROM `posts` as n
        JOIN `users` as u ON n.author_id = u.id WHERE n.id = ?',[$id]);
         $comments =  DB::table('Comments')
  
         ->select('Comments.id' ,'users.name' ,'Comments.text','Comments.created_at' ,'users.avatar','Comments.user_id')
         ->join('users', 'users.id', '=', 'Comments.user_id')
         ->join('posts', 'comments.post_id', '=', 'posts.id')
         ->where('posts.id', '=', $id)
        
        ->orderByDesc('Comments.created_at')
         ->paginate(8);
        
     
        
       if (isset($news[0])) {
           $view = $news[0]->views;
        $view = $view + 1;
        DB::update('update posts set views = ? where id = ?', [$view,$news[0]->id]);
        return view('school-news-single',['news'=>$news[0] ,'login'=>0,'comments'=>$comments]);
    }
    else {
        abort(404);
        
    }
    }
}
