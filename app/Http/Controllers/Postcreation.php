<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Postcreation extends Controller
{
   public function view(){
       $user = Auth::user();

       
        if ($user->role_id == 3  ) {
            $news = DB::table('posts')
  
       ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt','posts.school','posts.class' ,'posts.views')
       ->join('users', 'posts.author_id', '=', 'users.id')
       
       ->where('posts.author_id','=',$user->id)
      
      ->orderByDesc('posts.updated_at')
       ->paginate(5);
            return view('user-posts',['login'=>0,'new'=>'local','news'=>$news]);
        }
        elseif ($user->role_id == 4) {
            $news = DB::table('posts')
  
            ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt','posts.school','posts.class' ,'posts.views')
       ->join('users', 'posts.author_id', '=', 'users.id')
       
       
       ->where('posts.school','=',$user->school)
      ->orderByDesc('posts.updated_at')
       ->paginate(8);
            return view('user-posts',['login'=>0,'new'=>'local','news'=>$news]);
        }
        elseif ($user->role_id == 5) {
            $news = DB::table('posts')
  
       ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt' ,'posts.views')
       ->join('users', 'posts.author_id', '=', 'users.id')
       
       ->where('posts.school','=',0)
       ->where('posts.author_id','=',$user->id)
      ->orderByDesc('posts.updated_at')
       ->paginate(8);
            return view('user-posts',['login'=>0,'new'=>'global','news'=>$news]);
        }
        else{
            abort(401);
        }


   }
   public function delete(Request $request){
    $user = Auth::id();
    
    $news = DB::table('posts')
  
       ->select('users.id')
       ->join('users', 'posts.author_id', '=', 'users.id')
       
       ->where('posts.id','=',$request->post)->get();
    if($user == $news[0]->id){
        DB::table('posts')->where('id', '=', $request->post)->delete();
        
        return redirect('userposts');
    }
    else{
        abort(401);
    }
      

   }
   public function save($request){
        dd($request);
   }
}
