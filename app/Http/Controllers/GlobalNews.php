<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GlobalNews extends Controller
{
    public function all(){
       
       $news = DB::table('posts')
  
        ->select('users.name' , 'posts.title' ,'posts.updated_at','posts.image' ,'posts.id' ,'posts.excerpt' ,'posts.views')
        ->join('users', 'posts.author_id', '=', 'users.id')
        
        ->where('posts.school', 'IN', 0)
       
       ->orderByDesc('posts.updated_at')
        ->paginate(12);

        return view('news',['news'=>$news , 'login'=>0]);


    }
    public function single($id){
        
        $news = DB::select('SELECT  u.name , n.title ,n.updated_at,n.image ,n.id ,n.body,n.views,u.avatar FROM `posts` as n
        JOIN `users` as u ON n.author_id = u.id WHERE n.id = ?',[$id]);
       $view = $news[0]->views;
        $view = $view + 1;
      
     DB::update('update posts set views = ? where id = ?', [$view,$news[0]->id]);
    $comments =  DB::table('Comments')
  
     ->select('Comments.id' ,'users.name' ,'Comments.text','Comments.created_at' ,'users.avatar','Comments.user_id')
     ->join('users', 'users.id', '=', 'Comments.user_id')
     ->join('posts', 'comments.post_id', '=', 'posts.id')
     ->where('posts.id', '=', $id)
    
    ->orderByDesc('Comments.created_at')
     ->paginate(8);
     
       if (isset($news[0])) {
        return view('news-single',['news'=>$news[0] , 'login'=>0,'comments'=>$comments]);
    }
    else {
        abort(404);
        
    }
    }
}