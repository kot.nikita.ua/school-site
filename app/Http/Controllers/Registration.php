<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmEmail;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Str;

class Registration extends Controller
{
    public function reg($token){
    
      $email = DB::table('registers')->select('email')->where('token', '=', $token)->get();
      
      if(isset($email[0])){
        DB::table('registers')->where('token', '=', $token)->delete();
        return view('registration',['login'=>0,'email'=>$email]);
      }else{
       return abort(404);
      }
    }
    public function email(Request $request){
      $email = $request->input('Email');
      $check = DB::table('users')->select('email')->where('email', '=', $email)->get();
      $check2 = DB::table('registers')->select('email')->where('email', '=', $email)->get();
      
      if ( !isset($check2[0]) && !isset($check[0])) { 
        $token = Str::random(40);
        Mail::to($email)->send(new ConfirmEmail($token));
        
         DB::table('registers')->insert(
         ['email' => $email, 'token' => $token]
          );
        return redirect()->route('home',['message'=>'Заява на регістрацію відправленна перевірте пошту']);
        
      } else{
        return redirect()->route('home',['message'=>'Введенна пошта вже зареєстрована або подана в заявці на реєстрацію','style'=>'error']);
        
      
      }
        
        
        
       
    }
}
