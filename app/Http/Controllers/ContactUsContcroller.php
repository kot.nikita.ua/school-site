<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class ContactUsContcroller extends Controller
{
    public function main(Request $request) {
        if(View::exists('about')) {
            if(isset($request)) {
                $data = $request->validate([
                    'name' => 'required|min:3|max:20',
                    'email' => 'required|email|max:30',
                    'message' => 'required|min:25|max:300'
                ]);
                $admins = DB::table('users')->select('email')->where('role_id', '=', 1)
                    ->get();

                foreach($admins as $admin) {
                    Mail::to($admin->email)->send(new ContactUs($data));
                }
                return redirect()->route('about',['message'=>'Ваш відгук було відправленно']);
            }
        } else {
            abort(404);
        }
    }
}
