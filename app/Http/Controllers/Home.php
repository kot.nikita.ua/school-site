<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as FacadesRequest;

class Home extends Controller
{
    public function home(Request $request){
        
       if ($request->login == 'false') {
            $log = 1;
        } 
        else{
           $log = 0; 
        }
        
        $lastnews = DB::select("SELECT  title ,updated_at , image , id   FROM `posts` WHERE school = 0
        ORDER BY updated_at DESC LIMIT 3");
        if (isset($request->message)) {
            if (isset($request->style)) {
                return view('home',['login'=>$log,'news'=>$lastnews,'message'=>$request->message,'style'=>$request->style]);
            }else{
                return view('home',['login'=>$log,'news'=>$lastnews,'message'=>$request->message]);
            }
        }else{
            return view('home',['login'=>$log,'news'=>$lastnews]);
        }
    }
    public function about(Request $request){
        if (isset($request->message)) {
            if (isset($request->style)) {
                return view('about',['login'=>0,'message'=>$request->message,'style'=>$request->style]);
            }else{
                return view('about',['login'=>0,'message'=>$request->message]);
            }
        }else {
            return view('about',['login'=>0]);
        }
    }
    
}
