<?php

namespace App\Http\Controllers;

use App\Mail\Downgrade;
use App\Mail\upgradeAccept;
use App\Mail\upgradeDeny;
use App\Mail\upgradeRequest;
use App\Mail\upgradeRequire;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;


class User extends Controller
{
  public function settings(Request $request, $id)
  {

    if ($id == Auth::id()) {
      $user = DB::table('users')->where('id', $id)->first();
      $schools = DB::select('SELECT school FROM `posts` WHERE school != 0 ');
      $numbers = DB::select('SELECT Num FROM `schools`  ');
      if ($user->subcribes != 'all') {
        $schoolget = explode(',', $user->subcribes);
        $schoolget = array_unique($schoolget);
        $classs = DB::table('posts')->select('class')->whereIn('school', $schoolget)->get();
      } else {
        $classs = DB::table('posts')->select('class')->where('school', '!=', 0)->get();
      }

      $school[] = 'all';
      $class[] = 'all';
      foreach ($schools as $schoolnum) {

        $school[] = $schoolnum->school;
        $school = array_unique($school);
      }
      if ($user->subcribes == 'all') {
        $schoolsub[] = 'all';
      } else {
        $schoolsub = explode(',', $user->subcribes);
      }
      foreach ($schoolsub as  $value) {
        $keys = array_search($value, $school);
        if ($keys !== False) {
          $school[$keys] = "$school[$keys],cheked";
        }
      }

      foreach ($classs as $classnum) {

        $class[] = $classnum->class;
        $class = array_unique($class);
      }
      if ($user->class_sub == 'all') {
        $classsub[] = 'all';
      } else {
        $classsub = explode(',', $user->class_sub);
      }
      foreach ($classsub as  $value1) {
        $keys1 = array_search($value1, $class);
        if ($keys1 !== False) {
          $class[$keys1] = "$class[$keys1],cheked";
        }
      }
      $upgrade = DB::table('requests')

        ->select('users.name', 'requests.phone', 'requests.date', 'requests.role', 'requests.school', 'requests.id', 'users.avatar', 'users.email')
        ->join('users', 'requests.user_id', '=', 'users.id')


        ->where(

          'requests.user_id',
          '=',
          $id
        )->get();

      if (isset($upgrade[0])) {
        $req = true;
        if (isset($request->message)) {
          if (isset($request->style)) {
            return view('user', ['login' => 0, 'numbers' => $numbers, 'user' => $user, 'schools' => $school, 'classsub' => $class, 'upgrade' => $upgrade[0], 'req' => $req, 'message' => $request->message, 'style' => $request->style]);
          } else {
            return view('user', ['login' => 0,  'numbers' => $numbers, 'user' => $user, 'schools' => $school, 'classsub' => $class, 'upgrade' => $upgrade[0], 'req' => $req, 'message' => $request->message]);
          }
        } else {
          return view('user', ['login' => 0,  'numbers' => $numbers, 'user' => $user, 'schools' => $school, 'classsub' => $class, 'upgrade' => $upgrade[0], 'req' => $req]);
        }
      } else {
        $req = false;
        if (isset($request->message)) {
          if (isset($request->style)) {
            return view('user', ['login' => 0, 'user' => $user, 'numbers' => $numbers, 'schools' => $school, 'classsub' => $class, 'req' => $req, 'message' => $request->message, 'style' => $request->style]);
          } else {
            return view('user', ['login' => 0, 'user' => $user, 'numbers' => $numbers, 'schools' => $school, 'classsub' => $class, 'req' => $req, 'message' => $request->message]);
          }
        } else {
          return view('user', ['login' => 0, 'user' => $user, 'numbers' => $numbers, 'schools' => $school, 'classsub' => $class, 'req' => $req]);
        }
      }
    } else {
      redirect('/home');
    }
  }
  public function update(Request $request)
  {
    $user = Auth::user();

    $path = $request->file('image')->store('uploads', 'public');
    if ($user->avatar != 'users/default.png') {

      File::delete("storage/$user->avatar");
    }


    $id = Auth::id();
    DB::table('users')
      ->where('id', $id)
      ->update(['avatar' => $path]);
    return redirect()->route("user", ['message' => 'Збраження аватара зміннено', 'id' => $id]);
  }
  public function updateuser(Request $request)
  {
    $name = $request->name;
    $email = $request->email;
    $id = Auth::id();
    DB::table('users')
      ->where('id', $id)
      ->update(['name' => $name, 'email' => $email]);
    return redirect()->route("user", ['message' => 'Данні користувача зміннено', 'id' => $id]);
  }
  public function updatesub(Request $request)
  {
    $changed = false;
    $key = array_search('all', $request->option2);
    $user = Auth::user();

    if ($key === False) {

      $subcribes = implode(',', $request->option2);

      if ($user->subcribes != $subcribes) {
        DB::table('users')
          ->where('id', $user->id)
          ->update(['class_sub' => 'all']);
        $changed = true;
      }
      DB::table('users')
        ->where('id', $user->id)
        ->update(['subcribes' => $subcribes]);
    } else {
      $subcribes = 'all';
      if ($user->subcribes != $subcribes) {
        $changed = true;
        DB::table('users')
          ->where('id', $user->id)
          ->update(['class_sub' => 'all']);
      }
      DB::table('users')
        ->where('id', $user->id)
        ->update(['subcribes' => $subcribes]);
    }
    if ($changed == false) {
      return redirect()->route("user", ['message' => 'Підписки на школи були змінені', 'id' => $user->id]);
    } else {
      return redirect()->route("user", ['message' => 'Підписки на школи були змінені та відрізняются від попередніх. Підписки на класси були змінені на - всі', 'style' => 'warning', 'id' => $user->id]);
    }
  }
  public function updatesubclass(Request $request)
  {
    $key = array_search('all', $request->option2);
    $id = Auth::id();

    if ($key === False) {

      $subcribes = implode(',', $request->option2);

      DB::table('users')
        ->where('id', $id)
        ->update(['class_sub' => $subcribes]);
    } else {
      $subcribes = 'all';

      DB::table('users')
        ->where('id', $id)
        ->update(['class_sub' => $subcribes]);
    }

    return redirect()->route("user", ['message' => 'Підписки на класси були змінені', 'id' => $id]);
  }
  public function send(Request $request)
  {
    $user = Auth::user();
    switch ($request->role) {
      case 'teacher':
        $id = 4;
        $state = " Вчитель школи №" . $request->school;
        break;
      case 'admin':
        $id  = 5;
        $state = " Адміністратор школи №" . $request->school;
        break;
      case 'globaladmin':
        $id = 1;
        $state = "Глобальний адміністратор";
        break;
    }
    if ($request->role == 'globaladmin') {
      $school = 0;
      $schoolnum = 0;
    } elseif ($request->role == 'admin') {
      $school = $request->school;
      $schoolnum = 0;
    } else {
      $school = $request->school;
      $schoolnum = $request->school;
    }
    Mail::to($user->email)->send(new upgradeRequest($user->name, $request->phone, $state));
    $admins = DB::table('users')

      ->select('email')



      ->where(

        'role_id',
        '=',
        $id
      )->where('school', '=', $schoolnum)->get();

    foreach ($admins as  $admin) {
      Mail::to($admin->email)->send(new upgradeRequire($user->name, $request->phone, $state, $user->email));
    }


    DB::table('Requests')->insert(
      ['user_id' => $user->id, 'phone' => $request->phone, 'role' => $request->role, 'school' => $school]
    );
    return redirect()->route("user", ['message' => 'Ви подали заяву на Підвищення', 'id' => $user->id]);
  }
  public function upgrade()
  {
    $user = Auth::user();
    if ($user->role_id == 4) {
      $upgrade = DB::table('requests')

        ->select('users.name', 'requests.phone', 'requests.date', 'requests.role', 'requests.school', 'requests.id', 'users.avatar', 'users.email')
        ->join('users', 'requests.user_id', '=', 'users.id')


        ->where(

          'requests.role',
          '=',
          'teacher'
        )->paginate(5);
    } elseif ($user->role_id == 5) {
      $upgrade = DB::table('requests')

        ->select('users.name', 'requests.phone', 'requests.date', 'requests.role', 'requests.school', 'requests.id', 'users.avatar', 'users.email')
        ->join('users', 'requests.user_id', '=', 'users.id')


        ->where(

          'requests.role',
          '=',
          'admin'
        )->paginate(5);
    } elseif ($user->role_id == 1) {
      $upgrade = DB::table('requests')

        ->select('users.name', 'requests.phone', 'requests.date', 'requests.role', 'requests.school', 'requests.id', 'users.avatar', 'users.email')
        ->join('users', 'requests.user_id', '=', 'users.id')


        ->where(

          'requests.role',
          '=',
          'globaladmin'
        )->paginate(5);
    } else {
      abort(403, 'Unauthorized action.');
    }

    return view('user-requests', ['upgrades' => $upgrade, 'login' => 0]);
  }
  public function delete(Request $request)
  {
    $user = Auth::user();

    Mail::to($request->email)->send(new upgradeDeny());
    DB::table('requests')->where('id', '=', $request->post)->delete();
    return redirect("user/$user->id");
  }
  public function selfdelete(Request $request)
  {
    $user = Auth::user();


    DB::table('requests')->where('id', '=', $request->post)->delete();
    return redirect()->route("user", ['message' => 'Ви видалили заяву на підвищення', 'style' => 'warning', 'id' => $user->id]);
  }
  public function aprove(Request $request)
  {
    Mail::to($request->email)->send(new upgradeAccept());
    $id = DB::select("SELECT * FROM `requests` WHERE id = $request->post");
    if ($id[0]->role == 'teacher') {
      DB::table('users')
        ->where('id', $id[0]->user_id)
        ->update(['school' => $id[0]->school, 'role_id' => 3]);
    } elseif ($id[0]->role == 'admin') {
      DB::table('users')
        ->where('id', $id[0]->user_id)
        ->update(['school' => $id[0]->school, 'role_id' => 4]);
    } elseif ($id[0]->role == 'globaladmin') {
      DB::table('users')
        ->where('id', $id[0]->user_id)
        ->update(['role_id' => 5]);
    }
    DB::table('requests')->where('id', '=', $request->post)->delete();

    return redirect("user/upgrade");
  }
  public function seeteachers()
  {
    $user = Auth::user();
    switch ($user->role_id) {
      case 1:

        $admins = DB::table('users')->select('avatar', 'name', 'email', 'role_id', 'id', 'school')->where('role_id', '!=', 2)->where('role_id', '!=', 1)->paginate(10);
        break;
      case 4:
        $admins = DB::table('users')->select('avatar', 'name', 'email', 'id', 'role_id', 'school')->where('role_id', '=', 3)->where('school', '=', $user->school)->paginate(10);
        break;
      case 5:
        $admins = DB::table('users')->select('avatar', 'name', 'email', 'id', 'role_id', 'school')->where('role_id', '=', 4)->paginate(10);
        break;
    }



    return view('user-teachers', ['teachers' => $admins, 'login' => 0]);
  }
  public function downgradeteacher(Request $request)
  {
    switch ($request->role) {
      case 3:
        $send = "вчитель школи " . $request->school;
        break;
      case 4:
        $send = "адміністратор школи " . $request->school;
        break;
      case 5:
        $send = "глобальний адміністратор";
        break;
    }
    Mail::to($request->email)->send(new Downgrade($send));
    DB::table('users')
      ->where('id', $request->id)
      ->update(['role_id' => 2, 'school' => 0]);
    return redirect("/upgradedusers");
  }
  public function addschool(Request $request)
  {
    $user = Auth::user();
    $schools = DB::select("SELECT Num FROM `schools` WHERE Num = $request->num ");

    if ($schools == '' or $schools == null) {
      if ($request->num > 100000) {
        return redirect()->route("user", ['message' => 'Номер школи надто великий перевірте правильність набору', 'style' => 'warning', 'id' => $user->id]);
      }
      DB::table('schools')->insert(
        ['Num' => $request->num]
      );
      return redirect()->route("user", ['message' => 'Школа додана', 'id' => $user->id]);
    } else {
      return redirect()->route("user", ['message' => 'Така школа вже існує', 'style' => 'warning', 'id' => $user->id]);
    }
  }
  public function deleteschool(Request $request)
  {
    $user = Auth::user();
    DB::table('schools')->where('Num', '=', $request->school)->delete();
    return redirect()->route("user", ['message' => 'Школа видалена', 'id' => $user->id]);
  }
}
