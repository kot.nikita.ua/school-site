@component('mail::message')
# Вам відправили заяву на підвищення
Деталі заяви <br>
Ім'я -  {{$name}}<br>
телефон - {{$phone}}<br>
e-mail - {{$email}}<br>
Вибрана посада -  {{$state}}

@component('mail::button', ['url' => route('upgrade')])
Список заяв
@endcomponent

З повагою,<br>
{{ config('app.name') }}
@endcomponent
