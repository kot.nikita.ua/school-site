@component('mail::message')
# Новий відгук

До вас надійшов новий відгук з сайта Mentor!

@foreach ($data as $key => $value)
<tr style="background-color: #f8f8f8;">
<td style='padding: 10px; border: #e9e9e9 1px solid; text-align: center;'><b>{{$key}}</b></td>
<td style='padding: 10px; border: #e9e9e9 1px solid; text-align: center;'>{{$value}}</td>
</tr>
@endforeach

@component('mail::button', ['url' => route('home')])
    На головну
@endcomponent

З повагою,<br>
{{ config('app.name') }}
@endcomponent
