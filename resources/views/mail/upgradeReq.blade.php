@component('mail::message')
# Ви відправили заяву на підвищення
Деталі заяви <br>
Ім'я -  {{$name}}<br>
телефон - {{$phone}}<br>
Вибрана посада -  {{$state}}

@component('mail::button', ['url' => route('home')])
На головну
@endcomponent

З повагою,<br>
{{ config('app.name') }}
@endcomponent
