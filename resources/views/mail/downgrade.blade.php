@component('mail::message')
# Вас понизили 

з - {{$role}} <br>
до - користувача

@component('mail::button', ['url' => route('home')])
На головну
@endcomponent

З повагою,<br>
{{ config('app.name') }}
@endcomponent