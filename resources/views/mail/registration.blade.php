@component('mail::message')
# Первірка Електроної адресси

Ви подали заяву на реєстрацію

@component('mail::button', ['url' => route('reg', [$token])])
Перейти до реєстраціі
@endcomponent

З повагою,<br>
{{ config('app.name') }}
@endcomponent
