@component('mail::message')
# Заява про підвищення
Ваша заява була підтвердженна


@component('mail::button', ['url' => route('home')])
На головну
@endcomponent


З повагою,<br>
{{ config('app.name') }}
@endcomponent
