@component('mail::message')
# Заява про підвищення
Ваша заява була відклонена


@component('mail::button', ['url' => route('home')])
На головну
@endcomponent

З повагою,<br>
{{ config('app.name') }}
@endcomponent