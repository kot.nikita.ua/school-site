@extends('layouts.template')
@section('bread')
<li><a href="/school-news">Шкільні новини</a></li>
<li><a href="#">{{$news->title}}</a></li>
@endsection
@section('name')
{{$news->title}}
@endsection
@section('content')
<div class="courses-single-page" style="margin-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="site-content clearfix">
                    <article class="post post-blog-single">
                        <div class="featured-post post-img">
                            <img src="{{ asset('storage/')}}/{{$news->image}}" alt="images">
                        </div>
                        <div class="content-blog-single">
                            <ul class="social social-blog-single pd-top8">
                                <li><i class="fa fa-facebook" aria-hidden="true"></i></li>

                                <li><i class="fa fa-instagram" aria-hidden="true"></i></li>

                            </ul>
                            <div class="content-blog-single-inner">
                                <div class="content-blog-single-wrap">
                                    <h1 class="title pd-title-single">
                                        <a href="#"> {{$news->title}}</a>
                                    </h1>
                                    <!-- контент новости -->
                                    <? echo "{$news->body}"; ?>
                                    <!-- конец контента -->
                                    <div class="blog-single-poster">
                                        <div class="blog-single-poster-wrap">
                                            <div class="avtar-poster">
                                                <img src="{{ asset('storage/')}}/{{$news->avatar}}" alt="images"
                                                    style="width: 90px; height: 90px;border-radius: 90px; background-color:#028174">
                                            </div>
                                            <div class="info-poster">
                                                <div class="name">
                                                    {{$news->name}}
                                                </div>
                                                <div class="position">
                                                    {{date('j/n/Y', strtotime($news->updated_at))}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="refresh">
                                    @include('comments')
                                </div>
                                <div id="respond" class="comment-respond" style="margin-top: 50px;margin-bottom: 20px;">
                                    <h3 id="reply-title" class="comment-reply-title mg-bottom24">
                                        Коментувати
                                    </h3>
                                    @if(Auth::check())
                                    <div class="comments">

                                        <div class="message-wrap">
                                            <textarea name="comment" id="comment_text" rows="8"
                                                placeholder="Напишіть коментар"></textarea>
                                        </div>
                                        <div class="mg-top30">

                                            <button class="btn btn-post-comment box-shadow-type1" id="addComent">
                                                Залишити коментар
                                            </button>



                                        </div>
                                    </div>
                                    @else
                                    <div class="comments">

                                        <div class="message-wrap">
                                            <textarea name="comment" id="comment-message" rows="8"
                                                placeholder=" Зареэструйтесь щоб залишити коментар" disabled></textarea>
                                        </div>
                                        <div class="mg-top30">



                                            <button class="btn btn-post-comment box-shadow-type1" disabled>
                                                Зареэструйтесь щоб залишити коментар
                                            </button>

                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <script>var addd = document.getElementById('addComent');
 addd.onclick = function(){
    var text = $('#comment_text').val();
    var userid = "<? echo( Auth::id()); ?>";
    var postid = "{{$news->id}}";
    add(text,userid,postid);

 };
function add(Text,UserId,postId){
    
       $.ajax({
        
               url: "{{ route('addcoment') }}",
               method: "POST",
               contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data: { text: Text , user: UserId ,post: postId }
               
             })
               .done (

                   function (data){
                    
                     var sc = $('#refresh');
                        dn = $(sc).offset().top;
                        $('html, body').animate({scrollTop: dn}, 500);
                        update();
              
              }
              );          
 }
 function update(){
   var id = "{{$news->id}}";
   $.ajax({
    
           url: "{{ route('update') }}",
           method: "POST",
           contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           data: { id:id }
           
         })
           .done (

               function (data){
                
                $('#refresh').html(data);
               
          }
          );          
}
function deletecomment(coment_id){
   
   $.ajax({
    
           url: "{{ route('delete') }}",
           method: "POST",
           contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           data: { id:coment_id }
           
         })
           .done (

               function (data){
                
                update();
               
          }
          );          
}</script>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="related-posts" style="margin-bottom: 20px;" hidden>
                    <div class="title">
                        Схожі новини
                    </div>
                    <div class="flat-testimonial-carousel">
                        <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="2"
                            data-column2="2" data-column3="1" data-column4="1" data-dots="false" data-auto="false">
                            <div class="owl-carousel">
                                <!---------------------------------- похожая нвовость -->
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="{{ asset('assets/images/blog-single/02.png') }}" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">"Заголовок"</a>
                                                </h5>
                                                <div class="text">
                                                    "новость скороченно"
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Читати далі</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!---------------------------------- конец  похожей нвовости -->
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="{{ asset('assets/images/blog-single/03.png') }}" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">5 Tips To Improve Your Design Skills</a>
                                                </h5>
                                                <div class="text">
                                                    Tips To Improve Your Design Skills. Learn to identify good design.
                                                    If you want to create great designs, first you need to learn.
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Read more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="{{ asset('assets/images/blog-single/04.png') }}" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">5 Tips To Improve Your Design Skills</a>
                                                </h5>
                                                <div class="text">
                                                    Tips To Improve Your Design Skills. Learn to identify good design.
                                                    If you want to create great designs, first you need to learn.
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Read more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="post related-posts-wrap">
                                    <div class="post-border">
                                        <div class="featured-post">
                                            <img src="{{ asset('assets/images/blog-single/05.png') }}" alt="images">
                                        </div>
                                        <div class="content-related-post">
                                            <div class="related-post-inner">
                                                <h5 class="post-title lt-sp025">
                                                    <a href="#">5 Tips To Improve Your Design Skills</a>
                                                </h5>
                                                <div class="text">
                                                    Tips To Improve Your Design Skills. Learn to identify good design.
                                                    If you want to create great designs, first you need to learn.
                                                </div>
                                                <div class="read-more">
                                                    <a href="#">Read more</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar-right">
                    <!---------------------------------- можно спрятать -->
                    <div class="widget widget-class-start" hidden>
                        <div class="widget-title">
                            Початок Події
                        </div>
                        <div class="content">
                            <div class="flat-counter count-time" data-day="5" data-month="12" data-year="2020"
                                data-hour="20" data-minutes="00" data-second="00">
                                <div class="counter">
                                    <ul>
                                        <li class="content-counter">
                                            <div class="wrap-bg">
                                                <div class="inner-bg month">
                                                    <div class="numb-count numb cl-667eea">0</div>
                                                    <div class="name-count">Місяців</div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="content-counter">
                                            <div class="wrap-bg">
                                                <div class="inner-bg day">
                                                    <div class="numb-count numb cl-f0c41b">0</div>
                                                    <div class="name-count">Дні</div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="content-counter">
                                            <div class="wrap-bg">
                                                <div class="inner-bg hour">
                                                    <div class="numb-count numb cl-8b46f4">0</div>
                                                    <div class="name-count">Часи</div>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------------------------------- можно спрятать-->
                    <div class="widget widget-features">
                        <div class="widget-title">
                            Даталі Новини
                        </div>
                        <div class="content">
                            <ul class="features">
                                <li>
                                    <a href="#">Школа</a>
                                    <span>{{$news->school}}</span>
                                </li>

                                <li>
                                    <a href="#">Клас</a>
                                    <span>{{$news->class}}</span>
                                </li>

                            </ul>

                        </div>
                    </div>
                    @include('lastnews')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection