@component('mail::message')
# {{$title}}

{{$exert}}

@component('mail::button', ['url' => 'https://mentor/news/'.$id])
Читати далі
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
