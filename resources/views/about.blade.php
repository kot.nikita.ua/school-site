@extends('layouts.template')
@section('bread')
<li><a href="/about">Про проект</a></li>

@endsection
@section('name')
Про проект
@endsection
@section('content')
    <div id="about-developers" class="edukin-introduce equalize sm-equalize-auto clearfix">
        <div class="element-col50 bg-element1 element-text">
            <div class="title-section">
                <div class="flat-title medium heading-type18 text-dark">
                    Onion
                </div>
            </div>
            <p style="color:#333333;">
                Налаштуємо продажі через Instagram. Зробимо сайт або landing page.
                Телефон: +380999425174
            </p>
        </div>
        <div class="element-col50 bg-element2 element-bg">
            <div class="flat-spacer" data-desktop="0" data-mobi="400" data-smobi="300"></div>
        </div>
        <div class="element-col50 bg-element3 element-bg">
            <div class="flat-spacer" data-desktop="0" data-mobi="400" data-smobi="300"></div>
        </div>
        <div class="element-col50 bg-element4 element-text">
            <div class="title-section">
                <div class="flat-title medium heading-type19 text-dark">
                    IABS
                </div>
            </div>
            <p style="color:#333333;">
                IT курсы
                Навчаемо веб-програмуванню, веб-дизайну, граф-дизайну, робототехніці.
                Також є курсі для дітей до 15 років.
                Телефон: +380991939600
            </p>
        </div>
    </div><!-- edukin-introduce -->
    <div class="flat-team mg-flat-team">
        <div class="container">
            <div class="section-heading">
                <div class="title-section text-center">
                    <div class="flat-title medium heading-type20">Засновники поректа Mentor</div>
                </div>
                <p class="caption text-center">
                    Складний проект здається неможливим, поки він не є завершеним.
                </p>
            </div>
            <div class="pd-list-team">
                <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="3" data-column2="2" data-column3="1" data-column4="1" data-dots="false" data-auto="false" data-nav="false">
                    <div class="owl-carousel mx-auto h-auto">
                        <div class="team-box-layout-h1">
                            <div class="item-img">
                                <img src="{{ asset('assets/images/about/6.jpg') }}" alt="images" class="img-fluid">
                            </div>
                            <div class="item-content">
                                <div class="item-title">
                                    <a href="#">Микита Кот</a>
                                </div>
                                <div class="item-subtitle">Програміст</div>
                                <ul class="item-social">
                                    <li>
                                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-box-layout-h1">
                            <div class="item-img"> <img src="{{ asset('assets/images/about/4.jpg') }}" alt="images" class="img-fluid"></div>
                            <div class="item-content">
                                <div class="item-title">
                                    <a href="#">Єгор Чевардін</a>
                                </div>
                                <div class="item-subtitle">Програміст / Помаранчевий бізнесмен</div>
                                <ul class="item-social">
                                    <li>
                                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-box-layout-h1">
                            <div class="item-img"> <img src="{{ asset('assets/images/about/5.jpg') }}" alt="images" class="img-fluid"></div>
                            <div class="item-content">
                                <div class="item-title">
                                    <a href="#">Сергій Негодов</a>
                                </div>
                                <div class="item-subtitle">Співорганізатор, допомічник</div>
                                <ul class="item-social">
                                    <li>
                                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- flat-team -->
    <!-- contact form section start -->
    <div class="row justify-content-center bg-dark">
        <div id="form-container" class="col-md-4 shadow-lg m-5 p-5 text-light">
            <form id="contact-form" method="post" action="{{route('contact-us')}}" class="form-signin text-center">
                @csrf
                <img class="mb-4" src="{{asset('assets/images/logo/04.png')}}" alt="" width="72" height="72">
                <h1 class="h3 mb-3 font-weight-normal">Залишіть відгук</h1>
                <label for="inputEmail" class="sr-only">Ваша електронна адреса</label>
                <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Електронна адреса" required autofocus>
                <label for="inputName" class="sr-only">Ваше ім'я</label>
                <input name="name" type="text" id="inputName" class="form-control" placeholder="Ім'я" required>
                <label for="ContactMessage" class="sr-only">Залишіть ваше повідомлення</label>
                <textarea name="message" id="ContactMessage" class="form-control" placeholder="Ваше повідомлення" required></textarea>
                <button id="submit-button-contact" class="btn btn-dark btn-block" type="submit">Надіслати</button>
            </form>
        </div>
    </div>
    <!-- contact form section end -->
@endsection
