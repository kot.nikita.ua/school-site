@extends('layouts.template')
@section('bread')
<li><a href="/userposts">Мої новини</a></li>

@endsection
@section('name')
Мої новини
@endsection
@section('content')
<div class="container" id="fullsc">
    

<div class="col-70" style="margin-top: 20px; margin-bottom:20px;">

                    <div class="switch-layout" style="margin-bottom: 20px;">
                        
                        <button  onclick="window.location.href='/admin/posts/create'">Додати Новину<img src="{{ asset('assets\icons\plus.png') }}" alt="" style="margin-left:5px; height: 20px;width:20px; "></button>
                    </div>
                <div class="content-course-list">
                    @forelse($news as $newsone)
                    <div class="flat-course clearfix">
                        <div class="featured-post">
                            <div class="entry-image">
                                <img src="storage/{{$newsone->image}}" alt="images" style="height:270px;width:370px;">
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    @if($new == 'local')
                                    <a href="/school-news/{{$newsone->id}}">{{$newsone->title}}</a>
                                    @else
                                    <a href="/news/{{$newsone->id}}">{{$newsone->title}}</a>
                                    @endif
                                </h4>
                                <p>
                                    {{$newsone->excerpt}} 
                                </p>
                                <div class="author-info">
                                    <div class="author-name">
                                        {{$newsone->name}}
                                    </div>
                                    <div class="enroll">
                                        <a href="/admin/posts/{{$newsone->id}}/edit">Редагувати</a>
                                    </div>
                                </div>
                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">
                                        <form method="POST" action="{{ route('deletepost') }}">
                                            @csrf
                                            <button class="delete-post" name="post" value="{{$newsone->id}}">Видалити</button>
                                        </form>
                                        
                                    </div>
                                    <div class="price">
                                    @if($new == 'local')  
                                    <span class="price-now"><p id="school"> Школа №:<span> {{$newsone->school}} </span></p></span>
                                        <span class="price-now"><p id="class"> Клас: <span> {{$newsone->class}} </span></p></span>
                                        @else
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty
                        <p>Нема Новин</p>
                    @endforelse
                </div>
                {{ $news->links('layouts.pagination') }}
                
            </div>
</div>
@endsection
