<!-- Bootstrap-->
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/bootstrap.css') }}">
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css">
<script type="text/javascript" src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
<!-- Template Style-->
<link rel="stylesheet" href="{{ asset('assets/stylesheet/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/form.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/shortcodes.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/jquery-fancybox.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/flexslider.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/stylesheet/jquery.mCustomScrollbar.min.css') }}">

<!-- Favicon-->
<link href="{{ asset('assets/images/logo/favicon.ico') }}" rel="shortcut icon">
