<div class="pagination"> 
    <ul>
    <?
    
for ($i = 1; $i <= $paginator->lastPage(); $i++) {
    if($i == $paginator->currentPage()){
        ?>
            <li><a href="{{$paginator->url($i)}}" class="page-numbers current"  aria-disabled="true">{{$i}}</a></li>
        <?
    }else{
        ?>
            <li><a href="{{$paginator->url($i)}}" class="page-numbers">{{$i}}</a></li>
        <?
    }


}
?>

               </ul>
            </div>