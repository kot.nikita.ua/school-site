<div id="loading-overlay">
    <div class="loader"></div>
</div>
<div class="bg-header">
        <div class="flat-header-blog">
            <div class="top-bar clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            
                            
                        </div>
                        <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                            
                        </div>
                    </div>
                </div>
            </div>
            <header class="header menu-bar hv-menu-type2">
            <div class="container">
                <div class="menu-bar-wrap clearfix">
                    <div id="logo" class="logo">
                        <a href="{{ route('home') }}"><img src="{{ asset('assets/images/logo/02.png') }}" alt="mentor" style="width: 170px; height: 35px"></a>
                    </div>
                    <div class="mobile-button"><span></span></div>
                    <div class="header-menu">
                        <nav id="main-nav" class="main-nav">
                            <ul class="menu">
                                <li class="menu-item"><a href="{{ route('home') }}">Головна</a></li>
                                <li class="menu-item"><a href="{{ route('news') }}">Новини</a></li>
                                <li class="menu-item"><a href="{{ route('about-schools') }}">Школи</a></li>
                                @if(Auth::check())
                                    <li class="menu-item"><a href="{{ route('school-news') }}">Шкільні новини</a></li>
                                @endif
                                <li class="menu-item"><a href="{{ route('about') }}">Про проект</a></li>
                                @if (Route::has('login'))
                        @auth
                        <li><a href="">{{$user->name}} <img id="avatar" src="{{ asset('storage/')}}\{{ $user->avatar }}" alt="" style="width: 30px; height :30px;border-radius: 30px 30px 30px 30px; "></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="/user/{{Auth::id()}}">Налаштування</a></li>
                                            @if($user->role_id == 1)
                                            <li class="menu-item "><a href="/admin">Адмін панель</a></li>
                                            @endif
                                            @if($user->role_id == 4 Or $user->role_id == 5 Or $user->role_id == 1)
                                            <li class="menu-item "><a href="/user/upgrade">Заявки</a></li>
                                            @endif
                                            @if($user->role_id == 4 )
                                            <li class="menu-item "><a href="/about-schools/redact">Опис Школи</a></li>
                                            @endif
                                            @if($user->role_id == 4 Or $user->role_id == 5 Or $user->role_id == 1)
                                            <li class="menu-item "><a href="/upgradedusers">Список @if($user->role_id == 4)вчителів @endif @if($user->role_id == 5)адміністраторів @endif @if($user->role_id == 1)користувачів @endif</a></li>
                                            @endif
                                            @if($user->role_id == 3 OR $user->role_id == 4 Or $user->role_id == 5)
                                            <li class="menu-item "><a href="{{route('postsview')}}">Мої новини</a></li>
                                            @endif
                                            <li class="menu-item "><a href="/logout">Вихід</a></li>
                                        </ul><!-- sub-menu -->


                                    </li>

                    @else
                    <li class="menu-item"><a href="#" id="login">Увійти</a></li>
                      @endauth
                      @endif
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
            <div class="page-title page-title-blog">
                <div class="page-title-inner">
                    <div class="breadcrumbs breadcrumbs-blog text-left">
                        <div class="container">  
                            <div class="breadcrumbs-wrap">
                                <ul class="breadcrumbs-inner">
                                    <li><a href="/">Головна</a></li>
                                    @yield('bread')
                                </ul>
                                <div class="title">
                                @yield('name')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
