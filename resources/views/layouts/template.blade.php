<!DOCTYPE html>
<html lang="ua">
    <head>
        <meta charset="UTF-8">
        <title>Mentor</title>
        <!-- Mobile Specific Metas-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        @include('layouts.header-links')
    </head>
    <body>
        <?php
        $user = Auth::user()
        ?>
        <!-- validation errors section start -->
        @if($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert-danger p-5 text-center">{{ $error }}</div>
            @endforeach
        @endif
        @if(Session::has('msg'))
            <div class="alert-success p-5 text-center">{{ Session::get('msg') }}</div>
        @endif
        <!-- validation errors section end -->
        @include('message')
        @include('login')
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
        @include('layouts.footer-links')
    </body>
</html>
