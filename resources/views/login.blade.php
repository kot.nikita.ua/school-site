
<div class="loginup"  id="hid2">
<div id="hid" style="min-height: 100%;min-width:100%;z-index:200001;position:absolute;">

</div>
<div class="container2"  style=" padding-right: 0;
            padding-left: 0;
            ">
                    <div class="apply-admission bg-apply-type2">
                        <div class="apply-admission-wrap type1 bd-type2">
                            <div class="apply-admission-inner">
                                <h2 class="title text-center">
                                    <span>Увійти</span>
                                </h2>

                            </div>
                        </div>
                        <div class="form-apply">
                            <div class="section-overlay183251"></div>
                            <form  method="POST" action="{{ route('login') }}" class="apply-now">
                                @csrf
                                <ul>
                                    <li>
                                    <label for="email" style="color: white;" class="col-md-4 col-form-label ">{{ __('E-Mail Адресса') }}</label><input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

@error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror</li>
<li>
<label for="password" style="color: white;" class="col-md-4 col-form-label ">{{ ('Пароль') }}</label>
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  style="color:aliceblue;" required autocomplete="current-password">

@error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror</li>
                                </ul>
                                <div class="btn-50 hv-border text-center">
                                    <button type="submit"  class="btn bg-clfbb545">
                                       Увійти
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
</Div>
<script>
  var something = document.getElementById('hid');
something.onclick = function() {
 $("#hid2").hide(500);
};
                </script>
