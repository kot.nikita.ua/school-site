<div style="comments" >
    <h3 class="comments-h">
        Коментарі
    </h3>
    <ul>

        <!-- ------------ коментар -->
@forelse($comments as $comment)
        <li class="single_comment" id="full{{$comment->id}}">

            <div class="comment-content d-flex">

                <div class="comment-author">
                    <img src="{{ asset('storage/')}}/{{$comment->avatar}}" class="avatarpic"  alt="author">
                </div>

                <div class="comment-meta">
                    <div class="d-flex">
                        <a href="#" class="post-author">{{$comment->name}}</a>
                        <a href="#" class="post-date">{{date('j/n/Y', strtotime($comment->created_at))}} </a>
                        <!-- только для админов или етого пользавотеля -->
                        @if(Auth::id() == $comment->user_id)
                        <div class="delete">
                        
                            <input type="image" name="" value="" src="{{ asset('assets\icons\times-circle.svg') }}"
                                class="delete-ico">
                            <button type="button" id="{{$comment->id}}" class="delete-ico-span">Видалити коментар?</button>
                            <script>
                                 var com{{$comment->id}} = document.getElementById("{{$comment->id}}");
                                 com{{$comment->id}}.onclick = function(){
                                   $("#full"+"{{$comment->id}}").hide('fast',function(){
                                        deletecomment("{{$comment->id}}");
                                        });
                                    
                                    };
                                    
                            </script>
                        </div>
                        @endif


                        <!-- ------------ -->
                    </div>
                    <p>{{$comment->text}}</p>

                </div>
            </div>
        </li>
        @empty
        <li>
    <div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
        <p>Коментарів ще немає</p>
    </div></li>
    @endforelse
        <!-- ------------ конец коментара-->
    </ul>
</div>
<hr>
{{ $comments->links('layouts.pagination') }}