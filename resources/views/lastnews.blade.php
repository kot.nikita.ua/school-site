<div class="widget widget-latest-post">
    
<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (Auth::check()) {
    $user = Auth::user();
       if ($user->subcribes == 'all') {
            $schools2 = DB::select('SELECT school FROM `posts` WHERE school != 0 ');
            $school2[] = 0;
       foreach( $schools2 as $value2){
           
            $school2[] = $value2->school;
            $school2 = array_unique($school2);
           
            
       }; 
       $school2 = implode(',',$school2);
       }
       
       else{
           $school2 = "0,$user->subcribes";
       }
      
    $lastnews = DB::select("SELECT  title ,updated_at , image , id , school  FROM `posts` WHERE `school` IN ($school2)
     ORDER BY updated_at DESC LIMIT 4");
     
}
else {
    $lastnews = DB::select("SELECT  title ,updated_at , image , id ,school   FROM `posts` WHERE school = 0
    ORDER BY updated_at DESC LIMIT 4");
}
?>
                        <h4 class="widget-title">
                            <span>Останні Новини</span>
                        </h4>
                        <div class="latest-post">
                            <ul class="latest-post-wrap">
                                @forelse($lastnews as $onenews)
                                <!---------------------------------- новая новость -->
                                <li>
                                    <div class="thumb-new" style="margin: 5px;">
                                        <img src="{{ asset('storage/')}}/{{$onenews->image}}" style="width: 80px; height: 80px" alt="images">
                                    </div>
                                    <div class="thumb-new-content clearfix">
                                        <h6 class="thumb-new-title">
                                            <? if($onenews->school == 0){ ?>
                                            <a href="/news/{{$onenews->id}}">
                                                {{$onenews->title}}
                                            </a>
                                            <? }else{ ?>
                                            <a href="/school-news/{{$onenews->id}}">
                                                {{$onenews->title}}
                                            </a>
                                            <?}?>
                                        </h6>
                                        <p class="thumb-new-day">
                                        {{date('j/n/Y', strtotime($onenews->updated_at))}}
                                        </p>
                                    </div>
                                </li>
                                <!----------------------------------конец новой новости -->
                                @empty
                                <li> <p>Немае останніх новин</p> </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>