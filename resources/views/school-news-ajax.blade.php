<div class="site-content">
    <!-- класна новина -->
    @forelse($news as $content)
    <article class="post-blog box-shadow-type2">
        <div class="featured-post">
            <img src="storage/{{$content->image}}" alt="images">
        </div>
        <div class="content-post content-post-blog">
            <div class="post-meta">
                <div class="clendar-wrap">
                    <div class="day">
                        {{date('j', strtotime($content->updated_at))}}
                    </div>
                    <div class="month">
                        {{date('F', strtotime($content->updated_at))}}
                    </div>
                    <div class="day" style="margin-top: 5px;">
                        {{date('Y', strtotime($content->updated_at))}}
                    </div>
                </div>

            </div>
            <div class="content-post-inner">
                <div class="poster lt-sp0029">
                    Виклав :<span><a href="#">{{$content->name}}</a></span>
                </div>

                <h3 class="entry-title">
                    <a href="/school-news/{{$content->id}}" class="lt-sp0023">{{$content->title}}</a>
                </h3>
                <p>
                    {{$content->excerpt}}</p>
                <div class="flat-button">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="/school-news/{{$content->id}}">Читати Далі</a>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-3">
                            <p id="school"> Школа №:<span> {{$content->school}} </span></p>
                        </div>
                        <div class="col-md-3">
                            <p id="class"> Клас: <span> {{$content->class}} </span></p>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </article>
    @empty
    <div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
        <p>Нічого не знайдено</p>
    </div>
    @endforelse
    <!-- конец класной новине -->

</div>
@if($news->lastPage() !== 1)
<div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
    <button class="page-numbers current" id="addBlog" style="margin:20px">Загрузити більше</button></li>
</div>
<script>
var count = document.getElementById('addBlog');
count.onclick = function() {
    var text = $('#search').val();
    var value = Number("1");
    counter += value;
    
    search(text, counter, school3, clas3);
};
</script>
@endif