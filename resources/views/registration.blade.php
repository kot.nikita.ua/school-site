@extends('layouts.app')

@section('content')
<div style="min-width: 100%;min-height:100% ; ">
    

<div class="container3" >
    

                    <div class="apply-admission bg-apply-type2">
                        <div class="apply-admission-wrap type1 bd-type2">
                            <div class="apply-admission-inner">
                                <h2 class="title text-center">
                                    <span>Реестрація</span>
                                </h2>
                               
                            </div>
                        </div>
                        <div class="form-apply">
                            <div class="section-overlay183251"></div>
                            <form  method="POST" action="{{ route('register') }}" class="apply-now">
                                @csrf
                                <ul>
                                    <li> <label for="name" class="col-md-4 col-form-label " style="color: white;">{{ __("Ім'я") }}</label>


    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

    @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</li>
                                    <li> 
                                    <label for="email" style="color: white;" class="col-md-4 col-form-label ">{{ __('E-Mail Адресса') }}</label><input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email[0]->email }}" required autocomplete="email" autofocus>

@error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror</li>   
<li>
<label for="password" style="color: white;" class="col-md-4 col-form-label ">{{ ('Пароль') }}</label>
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  style="color:aliceblue;" required autocomplete="current-password">

@error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror</li>
<li><label for="password-confirm" class="col-md-4 col-form-label " style="color: white;">{{ __('Підтвердження паролю') }}</label>


    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
</li>
                           
                                </ul>
                                <div class="btn-50 hv-border text-center">
                                    <button type="submit"  class="btn bg-clfbb545">
                                      Зареєструватися
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div></div>
                </div>
@endsection
