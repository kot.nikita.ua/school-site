@extends('layouts.template')
@section('bread')
<li><a href="/user/upgrade">Заяви</a></li>

@endsection
@section('name')
Заяви на підвищення
@endsection
@section('content')
<div class="container" id="fullsc">
    

<div class="col-70" style="margin-top: 20px; margin-bottom:20px;">

                    
                <div class="content-course-list">
                    @forelse($upgrades as $upgrade)
                    
                       
                   
                   <div class="flat-course clearfix" style="padding: 20px;">
                        <div class="featured-post">
                            <div class="entry-image" style="margin-left: 20px;">
                                <img src="{{ asset('storage/')}}\{{ $upgrade->avatar }}" alt="images" style="height:100px;width:100px; border-radius:100px; background-color:#028174">
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                   
                                    <a href="#">Заява на підвищення</a>
                                   
                                </h4>
                                <p>
                                    Підвищення до : @if($upgrade->role == 'teacher') Вчителя Школи №:{{$upgrade->school}} @elseif($upgrade->role == 'admin')Адміністратор Школи №:{{$upgrade->school}}@elseif($upgrade->role == 'globaladmin') Глобальный Адміністратор @endif
                                </p>
                                <p>
                                    Деталі заяви: номер телефону - {{$upgrade->phone}} , Email - {{$upgrade->email}}
                                </p>
                                <div class="author-info">
                                    <div class="author-name">
                                        {{$upgrade->name}}
                                    </div>
                                    <div class="author-name" style="margin-left: 10px;">
                                       <span class="price-now">  {{date('j/n/Y', strtotime($upgrade->date))}}</span>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">
                                        <form method="POST" action="{{ route('deletereq') }}">
                                            @csrf
                                            <input type="text" name="email" value="{{$upgrade->email}}"  hidden>
                                            <button class="delete-post" type="submit" name="post" value="{{$upgrade->id}}" >Видалити Заяву</button>
                                        </form>
                                        
                                    </div>
                                    <div class="price">
                                    <form method="POST" action="{{ route('aprovereq') }}">
                                            @csrf
                                            <input type="text" name="email" value="{{$upgrade->email}}"  hidden>
                                            <button class="aprove-post"  type="submit" name="post" value="{{$upgrade->id}}" >Пітвердити Заяву</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                </div>
                    @empty
                        <p>Нема Заяв</p>
                    @endforelse
                </div>
                {{ $upgrades->links('layouts.pagination') }}
                
            </div>
</div>
@endsection
