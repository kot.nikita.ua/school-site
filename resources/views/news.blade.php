@extends('layouts.template')
@section('bread')
<li><a href="/news">Новини</a></li>
@endsection
@section('name')
Новини
@endsection
@section('content')

</div>
</div>

<!-- bg-header -->

<div class="course-grid">
    <div class="container">
        <div class="flat-portfolio">
            <ul class="flat-filter-isotype">
                <li>
                    <div style="color: #333333;">
                        <p>Сортувати :</p>
                    </div>
                </li>
                <li class="active">
                    <a href="#" id="date">За датою</a>
                </li>
                <li>
                    <a href="#" id="views">За Популярністю</a>
                </li>
            </ul>
            <div class="search-course">
                <div class="search-form">
                    <input type="search" placeholder="Пошук" id="reqest">
                    <button class="search-button" type="button" id="sear">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>






        <div id="refresh">
            @include('news-ajax')
        </div>









    </div>
</div>

<script>
var sear = document.getElementById('date');
var sear = document.getElementById('views');

var counter = Number("12");
var sort = 'date';
var sear = document.getElementById('sear');
sear.onclick = function() {
    var text = $('#reqest').val();

    search(text, counter, sort);

};

date.onclick = function() {
    var text = $('#reqest').val();
    var sort = 'date';

    search(text, counter, sort);

};
views.onclick = function() {
    var text = $('#reqest').val();
    var sort = 'views';

    search(text, counter, sort);

};



function search(search, counter, sort) {

    $.ajax({

            url: "{{ route('newsSearch') }}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                Search: search,
                addBlog: counter,
                Sort: sort
            }
        })
        .done(

            function(data) {


                $('#refresh').hide().html(data).fadeIn('slower');
            });
           
}
window.onload = function() {
    console.log('ready');
    search('', Number("12"), 'date');
}

</script>
<!-- course-grid -->
@endsection