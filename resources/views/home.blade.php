@extends('layouts.template2')
@section('content')
    <section class="campus">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                </div>
                <div class="col-lg-4">
                
                    <div class="apply-admission bg-apply-type2">
                        <div class="apply-admission-wrap type1 bd-type2">
                            <div class="apply-admission-inner">
                                <h2 class="title text-center">
                                    <span>Отримуй шкільні новини першим</span>
                                </h2>
                                <div class="caption text-center">
                                    Зроби свое життя більш легким!
                                </div>
                            </div>
                        </div>
                        <div class="form-apply">
                            <div class="section-overlay183251"></div>
                            <form action="{{ action('Registration@email') }}" method="POST" class="apply-now">
                                @csrf
                                @if(Auth::check())
                                <ul>
                                   
                                    <li><input type="email" name="Email" placeholder="Електронна пошта" required disabled></li>
                                
                                </ul>
                                <div class="btn-50 hv-border text-center">
                                    <button type="submit" name="Send" class="btn bg-clfbb545" disabled>
                                        Ви вже зарееєстровані
                                    </button>
                                </div>
                                @else
                                 <ul>
                                   
                                    <li><input type="email" name="Email" placeholder="Електронна пошта" required></li>
                                
                                </ul>
                                <div class="btn-50 hv-border text-center">
                                    <button type="submit" name="Send" class="btn bg-clfbb545">
                                        Зареєструватися
                                    </button>
                                </div>
                                 @endif
                            </form>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </section><!-- campus -->
    <section class="flat-introduce flat-introduce-style2 clearfix">
        <div class="container-fluid">
            <div class="section-about-left">
                <div class="videobox">
                    <!--<a class="fancybox" data-type="iframe">  if you have some tour video, you can uncomment this-->
                        <img src="{{ asset('assets/images/home/9.jpg') }}" alt="images">
                    <!--</a>-->
                </div>
            </div>
            <div class="section-about-right content-introduce">
                <div class="caption" id="how">
                    Як користуватися сайтом
                </div>
                <div class="title-section">
                    <div class="flat-title small heading-type1">Легше не буває</div>
                </div>
                <div class="content-introduce-inner">
                    <p>
                        Сайт допоможе вам слідкувати за всіми шкільними подіями та новинами, які відбуваються у Нікополі.

                        На сайті є міські новини, де публікються
                        глобальні новини кожної школи, наприклад закриття на карантин.
                    </p>
                    <p>
                        Також тут є локальні новини кожної школи для кожного класу. Щоб отримати доступ до них вам потрібно зарєеструватися.
                        Логакльні новини повідомляють вас про те, що коїться зараз у школі й класі, наприклад батьківьскі збори у 9-Б.
                        Після реєстрації вам буде надана можливість обрати потрібні школи і класи для повідомлень. Також буде запрошений доступ для відображення повідомлень - його треба прийняти.
                        Вчителю не потрібно лізти у вайбер і повідомляти кожно з батьків, ябо писати в группу й потім нагадувати ще раз, а ви отримаєте повідомення одразу після огоошення.
                    </p>
                    <div class="content-list">
                        <ul>
                            <li>
                                <span class="text">
                                    Введіть E-mail у форму та натиснить Зареєструватися.
                                </span>
                            </li>
                            <li>
                                <span class="text">
                                    У вас на пошті буде повідомлення про реєстрацію.
                                </span>
                            </li>
                            <li>
                                <span class="text">
                                    Натиснить на кнопку в письмі та заповніть форму реєстрації.
                                </span>
                            </li>
                            <li>
                                <span class="text">
                                    В своєму кабінеті відзначте галочки на школах а потім на класах, від яких ви бажаете отримувати повідомлення.
                                </span>
                            </li>
                            <li>
                                <span class="text">
                                    Якщо ви вчитель , натиснить Подати заяву на підвищення та виберіть посаду та школу і введіть ваш  номер телефону. 
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- flat-introduce -->
    <section class="flat-services-style2 clearfix">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg col-md">
                    <div class="flat-imagebox imagebox-services themesflat-content-box" data-padding="0% 0% 0% 3.6%" data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                        <div class="imagebox-content">
                            <img src="{{ asset('assets/images/home/12.png') }}" alt="images">
                            <h5 class="text-one text-white">Легкий у користуванні</h5>
                            <p class="text-white">
                                Все що вам потрібно зробити - зареєструватися, та вімкнути повідомення, про все отсанне ми подбаемо самі.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg col-md">
                    <div class="flat-imagebox imagebox-services themesflat-content-box" data-padding="0% 0% 0% 3%" data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                        <div class="imagebox-content">
                            <img src="{{ asset('assets/images/home/13.png') }}" alt="images">
                            <h5 class="text-two text-white">Завжди в курсі</h5>
                            <p class="text-white">
                                Вас повідомляють одразу після опублікування новини.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- flat-services -->
    <section class="latest-blog latest-blog-type2 latest-blog-style2">
        <div class="container">
            <div class="title-section">
                <div class="flat-title small heading-type7">
                    Глобальні новини
                </div>
            </div>
            <div class="row"> 
                @forelse($news as $new)
                <div class="col-lg-4 col-md-4">
                   
                    <article class="post post-style2 box-shadow-type1">
                        <div class="featured-post">
                            <img src="storage/{{$new->image}}" alt="images">
                        </div>
                        <div class="post-content">
                            <div class="category">
                            {{date('j/n/Y', strtotime($new->updated_at))}}
                            </div>
                            <div class="post-title">
                                <h5>
                                    <a href="/news/{{$new->id}}">{{$new->title}}</a>
                                </h5>
                            </div>
                            <div class="post-link">
                                <a href="/news/{{$new->id}}">Читати</a>
                            </div>
                        </div>
                    </article>
                   
                </div> 
                @empty
                    <p>Нема Новин</p>
                    @endforelse
            </div>
        </div>
     
    </section><!-- latest-blog -->
@endsection
