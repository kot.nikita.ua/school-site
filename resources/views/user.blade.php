@extends('layouts.template')
@section('bread')
<li><a href="#">Налаштування</a></li>

@endsection
@section('name')
Налаштування
@endsection
@section('content')

<div class="blog-bl content-blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="site-content">
                    <div class="user">
                        <div class="user_image" style="display: flex;
  align-items: center;
  justify-content: center ">
                            <img src="{{ asset('storage/')}}\{{ $user->avatar }}" alt=""
                                style="min-width: 200px;min-height :200px; height:201px;width:201px;border-radius: 200px; background-color:#028174 ">
                        </div>
                        <div class="user_name" style="display: flex;
  align-items: center;
  justify-content: center ">
                            <p>{{$user->name}}</p>
                            
                        </div>
                        <div class="user_name" style="display: flex;
  align-items: center;
  justify-content: center ; margin-top:5px">
                        @if($user->role_id == 1)
                            <p>Создатель</p>
                        @elseif($user->role_id == 2) 
                            <p>Користувач</p>
                        @elseif($user->role_id == 3) 
                            <p>Вчитель Школи №:{{$user->school}}</p>
                        @elseif($user->role_id == 4) 
                            <p>Адміністратор Школи №: {{$user->school}}</p>
                        @elseif($user->role_id == 5) 
                            <p>Глобальный Адміністратор</p>
                        @else
                            <p>Гість</p>
                        @endif 
                        </div>
                    </div>

                    <form enctype="multipart/form-data" action="{{ route('updatepic') }}" method="POST">
                        @csrf
                        <div style="display: flex;
  align-items: center;
  justify-content: center ">


                            <div class="changepic">

                                <ul>


                                    <li><input name="image" type="file" required/></li>
                                </ul>

                            </div>
                        </div>
                        
                        <div style="display: flex;
  align-items: center;
  justify-content: center; margin-bottom:20px ">
                            <input type="submit" value="Змінити аватар" />
                        </div>

                    </form>
                    @if( $user->role_id == 5)
                    <div style="display: flex;
  align-items: center;
  justify-content: center ">
  <div class="row">
  <div class="col-lg-12">
    <h3>Список Шкіл</h3>
  </div>
 
            <div class="col-lg-12">
            <form  action="{{ route('deleteschool') }}" method="POST">
                        @csrf
                        <select name="school" id="cars">
                Список шкіл
    @forelse($numbers as $number)
         
              <option value="{{$number->Num}}"> <p id="school"> Школа №:<span> {{$number->Num}} </span></p></option>
    @empty
                <option disabled> <p > Нема Шкіл </p></option>  
    @endforelse
                    
     
   </select>
        <button id="submit" style="margin-top: 20px;margin-bottom: 20px;">Видалити вибрану школу</button>
   </form>
            </div>
            <div class="col-lg-12">
    <h3>Додати школу</h3>
  </div>
            <div class="col-lg-12">
            <form  action="{{ route('addschool') }}" method="POST">
                        @csrf
                    <input type="number" name="num" size="10" maxlength="10" required placeholder="Виберіть номер Закладу">
                    <button id="submit" style="margin-top: 20px;">Додати школу</button>
            </form>
            </div>  
  </div>     
</div>   
                    @endif
                    @if($req == false)
                    @if($user->role_id == 2 Or $user->role_id == 3 Or $user->role_id == 4)
                    <div style="display: flex;
  align-items: center;
  justify-content: center ">
                    <button id="change">Подати заявку на підвищення</button>
</div>
                    <div class="container" style="margin-bottom: 20px;" id="upgrade">
                        
                    
                    <div class="widget widget-search" style="margin-top: 20px;">
                        <div class="search-blog-wrap">
                        <p style="color: black; font-weight:500; font-size:larger">Подати заявку на підвищення</p>
                            <form action="{{ route('send') }}" class="search-form" method="POST">
                            @csrf
                            <label for="role">Виберіть посаду:</label>

<select name="role" id="role" onchange="hid(this.value)">
                        @if($user->role_id == 2)
                          <option value="teacher" id="teacher">Шкільний Вчитель</option> 
                            <option value="admin" id = "admin">Шкільний Адміністратор</option>
                            <option value="globaladmin" id = "globaladmin">Глобальный Адміністратор</option> 
                        @elseif($user->role_id == 3) 
                            <option value="admin" id = "admin">Шкільний Адміністратор</option>
                            <option value="globaladmin" id = "globaladmin">Глобальный Адміністратор</option> 
                        @elseif($user->role_id == 4) 
                        <option value="globaladmin" id = "globaladmin">Глобальный Адміністратор</option> 
                        @endif

</select>            
                                <input type="tel" placeholder="Ваш телефон" name="phone" style="margin-top: 5px;"
                                     required>
                                     @if($user->role_id != 4)
                                     <div id="schoolchoise">
                                        
                                    
                                     <label for="cars">Виберіть школу:</label>

<select name="school" id="cars">
       
    <?php
        foreach($numbers as $number){
            ?> <option value="{{$number->Num}}"> <p id="school"> Школа №:<span> {{$number->Num}} </span></p></option><?
        }
    ?>
   <option value="0" id = "globalAdminOption" hidden>Глобальный Адміністратор</option> 
  
</select>    </div>
@endif         



                                <input type="submit" value="Відіслати"id="send" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>
                </div>
                   @endif
                   @else
                   
                   <div style="padding:20px; box-shadow: 0px 0px 20px 8px rgba(208, 208, 208, 0.2);margin-bottom:20px">
                       
                   
                   <div class="flat-course clearfix">
                        <div class="featured-post">
                            <div class="entry-image" style="margin-left: 20px;">
                                <img src="{{ asset('storage/')}}\{{ $upgrade->avatar }}" alt="images" style="height:100px;width:100px; border-radius:100px">
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                   
                                    <a href="#">Заява на підвищення відправленна перевірте електронну пошту</a>
                                   
                                </h4>
                                <p>
                                    Підвищення до : @if($upgrade->role == 'teacher') Вчителя Школи №:{{$upgrade->school}} @elseif($upgrade->role == 'admin')Адміністратор Школи №:{{$upgrade->school}}@elseif($upgrade->role == 'globaladmin') Глобальный Адміністратор @endif
                                </p>
                                <div class="author-info">
                                    <div class="author-name">
                                        {{$upgrade->name}}
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">
                                        <form method="POST" action="{{ route('selfdeletereq') }}">
                                            @csrf
                                            <button class="delete-post" type="submit" name="post" value="{{$upgrade->id}}" >Видалити Заяву</button>
                                        </form>
                                        
                                    </div>
                                    <div class="price">
                                    <span class="price-now">{{date('j/n/Y', strtotime($upgrade->date))}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   @endif 
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
               
                    <div class="widget widget-search" style="margin-top: 20px;">
                        <div class="search-blog-wrap">
                        <p style="color: black; font-weight:500; font-size:larger">Змінити Ім'я та Email</p>
                            <form action="{{ route('updateuser') }}" class="search-form" method="POST">
                            @csrf
                                <input type="email" placeholder="Email" name="email" style="margin-top: 5px;"
                                    value="{{$user->email}}" required>
                                <input type="text"  placeholder="Ім'я" name="name" style="margin-top: 5px;" value="{{$user->name}}"
                                    required>



                                <input type="submit" value="Зберегти" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>

                    <div class="widget widget-search">
                        <div class="search-blog-wrap">
                            <p style="color: black; font-weight:500; font-size:larger">Змінити підписки на школи</p>
                            <form action="{{ route('updatesub') }}" class="search-form" method="POST">
                            @csrf
                                <ul>
                                    <? 
                                            foreach($schools as $school){
                                                    $school = explode(',',$school);
                                                if ($school[0] == 'all') {
                                                  
                                               
                                                    if (isset($school[1])) {
                                                        ?>
                                                        <li><input type="checkbox" id="all" onclick="uncheckall()" placeholder="Email" name="option2[]" value="all" checked>
                                        <label for="subscribeNews">
                                            <p id="school"> Всі Школи </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                           <li><input type="checkbox" id="all" onclick="uncheckall()" placeholder="Email" name="option2[]" value="all" >
                                        <label for="subscribeNews">
                                            <p id="school"> Всі Школи </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }else {
                                                    if (isset($school[1])) {
                                                        ?>
                                                        <li><input type="checkbox" placeholder="Email" id="one" onclick="uncheckone()" name="option2[]" value="{{$school[0]}}" checked>
                                        <label for="subscribeNews">
                                            <p id="school"> Школа №:<span> {{$school[0]}} </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                          <li><input type="checkbox" placeholder="Email" id="one" onclick="uncheckone()" name="option2[]" value="{{$school[0]}}" >
                                        <label for="subscribeNews">
                                            <p id="school"> Школа №:<span> {{$school[0]}} </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }

                                             
                                            }
                                        ?>
                                    
                                           
                                            
                                    
                                            
                                        
                                </ul>


                                <input type="submit" value="Зберегти" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>
                    <div class="widget widget-search">
                        <div class="search-blog-wrap">
                            <p style="color: black; font-weight:500; font-size:larger">Змінити підписки на класси</p>
                            <form action="{{ route('updatesubclass') }}" class="search-form" method="POST">
                            @csrf
                                <ul>
                                    <? 
                                   
                                            foreach($classsub as $class){
                                                    $class = explode(',',$class);
                                                if ($class[0] == 'all') {
                                                    
                                               
                                                    if (isset($class[1])) {
                                                        ?>
                                                        <li><input type="checkbox" id="all1" onclick="uncheckall2()" placeholder="Email" name="option2[]" value="all" checked>
                                        <label for="subscribeNews">
                                            <p id="class"> Всі класси </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                           <li><input type="checkbox" id="all1" onclick="uncheckall2()" placeholder="Email" name="option2[]" value="all" >
                                        <label for="subscribeNews">
                                            <p id="class"> Всі класси </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }else {
                                                    if (isset($class[1])) {
                                                        ?>
                                                        <li><input type="checkbox" placeholder="Email" id="one1" onclick="uncheckone2()" name="option2[]" value="{{$class[0]}}" checked>
                                        <label for="subscribeNews">
                                            <p id="class"> Класс №:<span> {{$class[0]}} </span></p>
                                        </label>
                                    </li>
                                                        <?
                                                    }
                                                    else{

                                                        ?>
                                                          <li><input type="checkbox" placeholder="Email" id="one1" onclick="uncheckone2()" name="option2[]" value="{{$class[0]}}" >
                                        <label for="subscribeNews">
                                            <p id="class"> Класс №:<span> {{$class[0]}} </span></p>
                                        </label>
                                    </li>
                                                        
                                                        <?
                                                    }
                                                }

                                             
                                            }
                                        ?>
                                    
                                           
                                            
                                    
                                            
                                        
                                </ul>


                                <input type="submit" value="Зберегти" style="margin-top: 5px;" />
                            </form>
                        </div>
                    </div>                        
                   
                        </div>
 
            </div>
        </div>
    </div>
</div><!-- content-blog -->
<script>
    function hid(role){
        if (role === "globaladmin") {
           $('#schoolchoise').hide();
        }
        else{
            $('#schoolchoise').show();
        }
        
    }
    
    function uncheckall(){
        var checkBox = document.getElementById("all");
        if (checkBox.checked == true) {
            uncheckElements();
            checkBox.checked=true;
        }
    }
    function uncheckone(){
        
        
       
            var checkBox2 = document.getElementById("all"); 
            checkBox2.checked=false;
       
    }
    function uncheckall2(){
        var checkBox = document.getElementById("all1");
        if (checkBox.checked == true) {
            uncheckElements1();
            checkBox.checked=true;
        }
    }
    function uncheckone2(){
        
        
       
            var checkBox2 = document.getElementById("all1"); 
            checkBox2.checked=false;
       
    }
    function uncheckElements()
{
 var uncheck=document.getElementsByTagName('input');
 for(var i=0;i<uncheck.length;i++)
 {
  if(uncheck[i].type=='checkbox')
  {
    if(uncheck[i].id=='one'){
        uncheck[i].checked=false;
    }
  }
 }
}
function uncheckElements1()
{
 var uncheck=document.getElementsByTagName('input');
 for(var i=0;i<uncheck.length;i++)
 {
  if(uncheck[i].type=='checkbox')
  {
    if(uncheck[i].id=='one1'){
        uncheck[i].checked=false;
    }
  }
 }
}
</script>
@isset ($path)
<img src="{{asset('/storage/' . $path)}}" alt="">
@endisset
@endsection