@extends('layouts.template')
@section('bread')
<li><a href="/about-schools">Школи</a></li>

@endsection
@section('name')
Школи
@endsection
@section('content')
<div id="fullsc">
<div class="blog-bl content-blog" style="margin-bottom: 40px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-content">
                    @forelse($schools as $school)
                        <article class="post-blog box-shadow-type2">
                            <div class="featured-post">
                                <img src="{{ asset('storage/')}}/{{$school->image}}" alt="images" style="width: 100%;">
                            </div>
                            <div class="content-post content-post-blog">
                                <div class="post-meta">
                                    <div class="clendar-wrap">
                                        <div class="day">
                                        {{$school->schoolNum}}
                                        </div>
                                        <div class="month">
                                          №
                                        </div>
                                    </div>
                                    <ul class="social" hidden> 
                                        <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                        
                                        <li><i class="fa fa-instagram" aria-hidden="true"></i></li>
                                        
                                    </ul>
                                </div>
                                <div class="content-post-inner">
                                   
                                    <h3 class="entry-title">
                                        <a href="/about-schools/{{$school->id}}" class="lt-sp0023">{{$school->title}}</a>
                                    </h3>
                                    <p>
                                    {{$school->excerpt}}
                                    </p>
                                    <div class="flat-button">
                                        <a href="/about-schools/{{$school->id}}">Дізнатися більше</a>
                                    </div>
                                </div>
                            </div>
                        </article>
                        @empty
                        <p>Нема Заяв</p>
                    @endforelse
                    </div>
                    {{ $schools->links('layouts.pagination') }}
                </div>
               
            </div>
        </div>
    </div><!-- content-blog -->
</div> 
@endsection