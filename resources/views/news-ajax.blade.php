<?

function simpl($num){
    if ($num > 1000 && $num < 1000000) {
        $num1 = $num % 1000;
        $simpl = $num1."K";
    }elseif($num >1000000){
        $num1 = $num / 1000000;
        $num1 = round($num1 , 2);
        $simpl = $num1."M";
    }else{
        $simpl = $num;
    }
    return $simpl;
}

?>
<div class="flat-courses clearfix isotope-courses" >
<div class="row align-center mt-5 mr-1 ml-1" >
                @forelse($news as $content) 
                <div class="course clearfix Marketing Certificate Popular" >
               
                    <div class="flat-course" >
                        <div class="featured-post post-media">
                            <div class="entry-image pic">
                                <img src="storage/{{$content->image}}" alt="images">
                                <div class="hover-effect"></div>
                                <div class="links">
                                    <a href="/news/{{$content->id}}">Детальніше</a>
                                </div>
                            </div>
                        </div>
                        <div class="course-content clearfix" >
                            <div class="wrap-course-content" >
                                <h4>
                                    <a href="/news/{{$content->id}}">{{$content->title}}</a>
                                </h4>
                                <p>
                                    {{$content->excerpt}}
                                </p>

                            </div>
                            <div class="wrap-rating-price">
                                <div class="meta-rate">
                                    <div class="rating">

                                        <span>{{$content->name}}</span>
                                    </div>
                                    <div class="price">
                                    <span class="price-previou">
                                            <img src="{{ asset('assets/icons/vision.png')}}" style="height: 25px; width:25px; margin-bottom:1px; opacity:0.5;"><span>:<?echo simpl($content->views) ?></span>
                                        </span>|
                                        <span class="price-now">{{date('j/n/Y', strtotime($content->updated_at))}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>

                @empty
    <div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
        <p>Нічого не знайдено</p>
    </div>
    @endforelse

</div>
</div> 
@if($news->lastPage() !== 1)
<div class="container" style="display: flex;
  align-items: center;
  justify-content: center ">
    <button class="page-numbers current" id="addBlog" style="margin:20px">Загрузити більше</button></li>
</div>
<script>
var count = document.getElementById('addBlog');
count.onclick = function() {
        var text = $('#reqest').val();
       var value = Number("12");
       counter += value;
         
     search(text,counter,sort);
 };
</script>

@endif          
            